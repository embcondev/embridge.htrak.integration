# Allow skipping of database steps.
param (
    [string]$milestone = '',
    [switch]$h = $false,
    [switch]$Database = $true,      # Include in the database ?
    [string]$ubwDsn = '',           # Data source name for database connection
    [string]$client = '',           # client where the package will be installed
    [switch]$copyToBin = $false,
    [string]$actStat = 'L',
    [switch]$actKeep = $false,      
    [switch]$iisreset = $false
)

# Changes the user preference for the PowerShell execution policy.
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass -Force

<# *****************
* Module Variables *
******************** #>
$global:innerError = 0

# Write a string to an output and place the output in a log file
Function WriteToOutput([string]$text)
{
    if (!$logFile)
    {
        $root = Split-Path -Parent $MyInvocation.MyCommand.Definition
        $logFile = Get-Item $MyInvocation.MyCommand.Definition
        $logFile = ($root + "\" + $logFile.BaseName + "-" + (Get-Date -Format "yyyyMMdd-HHmmss") + ".log")
    }

    Write-Output $text | Tee-Object -FilePath $logFile -Append
}

# Function to copy files to target folder
Function CopyFiles ([string]$Extension, [string]$TargetPath, [string]$File)
{
    foreach ($item in Get-ChildItem -Path ".\" -Filter $Extension)
    {
        $itemName = ("'" + $item.Name + "'")
        
        if ($item.VersionInfo.FileVersion -ne $null)
        {
            $itemName += (" (" + $item.VersionInfo.FileVersion + ")")
        }

        Write-Verbose -Verbose ("Copy " + $itemName + " to '" + $TargetPath + "'") *>&1 | Tee-Object -FilePath $File -Append
        Copy-Item $item.FullName "$TargetPath" -Force -ErrorAction Stop *>&1 | Tee-Object -FilePath $File -Append
    }
}

# Import the ACT assemblies into the database
Function ImportAct ()
{
    param (
        [string]$argAgressoExe,    # The value of the 'AGRESSO_EXE' environment variable
        [string]$argUbwDsn,        # The value of the main script '-dsn:' parameter
        [string]$argAgressoCustom, # The value of the 'AGRESSO_CUSTOM' environment variable
        [string]$argActStat,       # The value of the main script '-actStat' parameter
        [bool]$argActKeep,         # The value of the main script '-actKeep' parameter
        [string]$argLogFile        # The path of the log file
    )

    try
    {
        if (-not ($argActKeep))
        {
            # In theory there shouldn't be anything in the 'ACT' folder except dlls, but...
            foreach ($item in Get-ChildItem -Path ".\" -Filter "*.dll") {
                # For some reason the actsetup /c option won't do anything if you supply it with the full dll name
                [string]$tempName = $item.Name.replace(".dll", "*")
                Write-Output "Clearing assembly: $item" | Tee-Object -FilePath $argLogFile -Append
                & ($argAgressoExe + "actsetup.exe") /D:$argUbwDsn /c:$tempName | Tee-Object -FilePath $argLogFile -Append
            }
        }
        # In theory there shouldn't be anything in the 'ACT' folder except dlls, but...
        foreach ($item in Get-ChildItem -Path ".\" -Filter "*.dll") {
            try
            {
                Write-Output "Importing assembly: $item" | Tee-Object -FilePath $argLogFile -Append
                & ($argAgressoExe + "actsetup.exe") /D:$argUbwDsn /u /lib /s:$argActStat ".\$item" | Tee-Object -FilePath $argLogFile -Append
            }
            catch
            {
                Write-Output "Error during ImportAct"
                Write-Warning $Error[0] | Tee-Object -FilePath $logFile -Append
                $exitApp = Read-Host "Close Installer? [$($defaultValue)]"
                $exitApp = ($defaultValue,$exitApp)[[bool]$exitApp]
                if ($exitApp.ToString().ToUpper() -eq 'Y')
                {
                    $global:innerError = 1
                    throw [System.Exception]
                }
            }
        }
    }
    catch 
    {
        if($global:innerError -eq 1)
        {
            throw [System.Exception]
        }
        else
        {
            Write-Warning $Error[0] | Tee-Object -FilePath $logFile -Append
            $exitApp = Read-Host "Close Installer? [$($defaultValue)]"
            $exitApp = ($defaultValue,$exitApp)[[bool]$exitApp]
            if ($exitApp.ToString().ToUpper() -eq 'Y')
            {
                $global:innerError = 1
                throw [System.Exception]
            }
        }

    }
}

# Get the path to the Web Services 'Bin' folder
Function RetrieveWebServiceBin()
{
    param (
		[ref]$argWebSvcPath             # The path of the Web Services Bin folder
	)
    
    [string]$argWebPath_ = [string]::Empty

    if ($milestone -lt 7) {
        RetrieveWebPath -argWebPath ([ref]$argWebPath_) -settingsType "Agresso Web Services"
    } else {
        RetrieveWebPath -argWebPath ([ref]$argWebPath_) -settingsType "Business World Web Services"    
    }

    $argWebSvcPath.value = ($argWebPath_ + "\bin")
}

# Get the path to the Web Api 'Bin' folder
Function RetrieveWebApiBin()
{
    param (
		[ref]$argWebBinApiPath          # The path of the Web Api\Bin folder
	)
    
    [string]$argWebPath_ = [string]::Empty

    if ($milestone -lt 7) {
        RetrieveWebPath -argWebPath ([ref]$argWebPath_) -settingsType "Agresso Web Api Server"
    } else {
        RetrieveWebPath -argWebPath ([ref]$argWebPath_) -settingsType "Business World Web Api Server"
    }

    $argWebBinApiPath.value = ($argWebPath_ + "\bin")
}

# Get the path to the Web 'bin' and 'custom' folders
Function RetrieveWebBinAndCustom()
{
    param (
		[ref]$argWebBinPath,            # The path of the Web\bin folder
        [ref]$argWebCustomPath          # The path of the Web\custom folder
	)
    
    [string]$argWebPath_ = [string]::Empty

    if ($milestone -lt 7) {
        RetrieveWebPath -argWebPath ([ref]$argWebPath_) -settingsType "Agresso Web"
    } else {
        RetrieveWebPath -argWebPath ([ref]$argWebPath_) -settingsType "Business World Web"
    }

    $argWebBinPath.value = ($argWebPath_ + "\bin")
    $argWebCustomPath.value = ($argWebPath_ + "\custom")
}

Function RetrieveWebPath()
{
	param (
        [string]$settingsType,
        [ref]$argWebPath                # The path of the Physical Folder
	)
	# This reads the value of user-defined global variable: '$ubwBds'
	if (-not (Get-Command "New-AgrDsDrive" -errorAction SilentlyContinue))
	{
		#region Load the Agresso PowerTools features to use Agresso.Management.PowerShell
		Import-Module -Name "AgressoPowerTools"
		Import-AgressoModule
		#endregion
	}
	# Create a virtual-drive that represents the Backoffice Data Source
	New-AgrDsDrive -DataSource $ubwDsn -LoginType None | out-null
	[string]$webSitePath = ($ubwDsn + ":\Features\WebApplications (x64)\Default Web Site")
	cd $webSitePath
	foreach ($webApp in Get-ChildItem -Path ".\") {
		if ($webApp.PSIsContainer) {
			$type = get-item (".\" + $webApp.PSChildName + "\AppSettings\Type")
            if ($type.Value -eq $settingsType) {
                $argWebPath.value = $webApp.PhysicalFolder
				break
			}
		}
	}
	cd $root
}

# Clear any existing PowerShell errors.
$error.Clear()


<# ******************
* The '-h' argument *
********************* #>
if ($h)
{
    "Description"
    "==========="
    "Install customizations for UNIT4 Business World."
    ""
    "Expected folders structure in the current directory:"
    " - ACT"
    " - Customised Reports"
    " - Data Import"
    " - Database Scripts"
    " - Stylesheets"
    " - TopGen"
    " - Web Services Bin"
    ""
    "Notes: Empty folders are not mandatory."
    "       Database Scripts folder requests an asql.lst file with the list of scripts included."
    "       Database Scripts folder should have a 'update_version_information.asq' file and it should be "
    "       the last one to run. Make sure you put it in last, in the asql.lst file."
    ""
    "+===================+===========+==================================================================+"
    "| Parameter         | Default   | Meaning                                                          |"
    "+===================+===========+==================================================================+"
    "| -milestone        | None      | Milestone version (example: 7)                                   |"
    "+-------------------+-----------+------------------------------------------------------------------+"
    "| -h                | 0 (false) | Display this help and exit                                       |"
    "+-------------------+-----------+------------------------------------------------------------------+"
    "| -Database         | 1 (true ) | Install to database?                                             |"
    "+-------------------+-----------+------------------------------------------------------------------+"
    "| -ubwDsn:Xxxxxxxxx | None      | Where Xxxxxxxxxxx is the name of the Backoffice Data Source Name |"
    "|                   |           | (DSN) to install into.                                           |"
    "+-------------------+-----------+------------------------------------------------------------------+"
    "| -client:XX        | *         | Where XX is the client code in which the package will be         |"
    "|                   |           | installed. This value will be injected into each ASQL script.    |"
    "+-------------------+-----------+------------------------------------------------------------------+"
    "| -copyToBin        | 0 (false) | Copy the ACT assemblies to the UBW Bin folder?                   |"
    "+-------------------+-----------+------------------------------------------------------------------+"
    "| -actStat:X        | L         | Where X is the status setting for the imported ACT assemblies,   |"
    "|                   |           | i.e. this value will be used as the value of parameter ""/s:"" in|"
    "|                   |           | 'actsetup.exe':                                                  |"
    "|                   |           |    N = Normal load                                               |"
    "|                   |           |    C = Closed                                                    |"
    "|                   |           |    F = Always file                                               |"
    "|                   |           |    L = Always database                                           |"
    "|                   |           |    D = Deploy (database to file)                                 |"
    "|                   |           |    S = Secure load                                               |"
    "+-------------------+-----------+------------------------------------------------------------------+"
    "| -actKeep          | 0 (false) | Do not clear the previous ACT import?                            |"
    "|                   |           | N.B. Due to a bug in 'actsetup.exe' if this parameter is set     |"
    "|                   |           |      then each time you run this script a new version of each    |"
    "|                   |           |      assembly will be added to the ACT tables. This will cause   |"
    "|                   |           |      problems if the TCT001 screen has been used to configure    |"
    "|                   |           |      any of the ACT classes to only run on certain menu IDs,     |"
    "|                   |           |      report variants, etc.                                       |"
    "+-------------------+-----------+------------------------------------------------------------------+"
    "| -iisreset         | 0 (false) | Perform an IISReset in the end                                   |"
    "+===================+===========+==================================================================+"
    ""
    exit
}


#region Variable Definition

$curr = get-location
$root = Split-Path -Parent $MyInvocation.MyCommand.Definition
$logFile = Get-Item $MyInvocation.MyCommand.Definition
$logFile = ($root + "\" + $logFile.BaseName + "-" + (Get-Date -Format "yyyyMMdd-HHmmss") + ".log")

#endregion

try
{
    cd ($root)
    if (Test-Path "release_notes.txt")
    {
        foreach($row in Get-Content "release_notes.txt") {
            $patternFound = $row | select-string -pattern "version#"
            if ($patternFound)
            {
                $versionFromFile = $patternFound | %{ $_.line.split(":")[1].Trim() }
                
                if ([string]::IsNullOrEmpty($versionFromFile))
                {
                    throw "No milestone has been specified in the release notes. Use version#: <milestone>. Installation halted."
                }
                
                if ($milestone -and ($milestone -ne $versionFromFile))
                {
                    throw "Milestone " + $milestone + " does not match the version found in release notes. Installation halted."
                }
                
                $milestone = $versionFromFile
            }
        }
    }
    else
    {
        if ([string]::IsNullOrEmpty($milestone))
        {
            throw "No milestone has been specified. Use parameter -milestone <version>. Installation halted."
        }
    }
    
    WriteToOutput("Milestone: " + $milestone)
    
    $path32 = "HKLM:\SOFTWARE\Wow6432node\UNIT4\Business World " + $milestone + ".0.0"
    if ($milestone -lt 6)
    {
        $path32 = "HKLM:\SOFTWARE\Wow6432node\UNIT4\Agresso "+ $milestone
    }
    
    $path64 = "HKLM:\SOFTWARE\UNIT4\Business World " + $milestone + ".0.0"
    if ($milestone -lt 6)
    {
        $path64 = "HKLM:\SOFTWARE\Wow6432node\UNIT4\Agresso "+ $milestone
    }
        
    if (!(Test-Path $path32) -and !(Test-Path $path64))
    {
        throw "Milestone " + $milestone + " not installed in this environment. Installation halted."
    }

    if (Test-Path $path32)
    {
        $key = Get-ItemProperty -Path $path32 -Name Version
        $versionFromRegistry = $key.Version
        $versionFromRegistry = $versionFromRegistry.Substring(0,1)

        if (($milestone -lt 6 -and ($milestone -ne $key.Version)) -or ($milestone -ge 6 -and ($milestone -ne $versionFromRegistry)))
        {
            throw "Milestone " + $milestone + " not installed in this environment. Installation halted."
        }                
    }
    
    if (Test-Path $path64)
    {
        $key = Get-ItemProperty -Path $path64 -Name Version
        $versionFromRegistry = $key.Version
        $versionFromRegistry = $versionFromRegistry.Substring(0,1)

        if (($milestone -lt 6 -and ($milestone -ne $key.Version)) -or ($milestone -ge 6 -and ($milestone -ne $versionFromRegistry)))
        {
            throw "Milestone " + $milestone + " not installed in this environment. Installation halted."
        }         
    }

    if ($actStat -eq '')
    {
        $actStat = 'L'
    }

    # Ensure we are running PowerShell version 3.0 or higher.
    if ($PSVersionTable.PSVersion.Major -lt 3)
    {
        throw "PowerShell version 3.0 or higher is required to run this script."
    }

    # Ensure we are running as an Administrator.
    if (-not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
    {
        throw "Administrator rights are required to run this script."
    }

    $defaultValue = 'N'

    if($Database)
    {
        if ([string]::IsNullOrEmpty($ubwDsn))
        {
            # Prompt user for datasource name.
            $ubwDsn = Read-Host "Enter the UBW Backoffice Data Source Name"
        }

        WriteToOutput("DSN: " + $ubwDsn)
        
        # Find Agresso environment variables.
        $path = "HKLM:\SOFTWARE\Wow6432node\UNIT4\Business World " + $milestone + ".0.0\Data Sources\" + $ubwDsn + "\Business Server Environment"
        if ($milestone -lt 6)
        {
            $path = "HKLM:\SOFTWARE\Wow6432node\UNIT4\Agresso "+ $milestone + "\Data Sources\" + $ubwDsn + "\Business Server Environment"
        }

        if (Test-Path $path)
        {
            $key = Get-ItemProperty -Path $path -Name AGRESSO_EXE
            $agresso_exe_x86 = $key.AGRESSO_EXE
            $key = Get-ItemProperty -Path $path -Name AGRESSO_IMPORT
            $agresso_import_x86 = $key.AGRESSO_IMPORT
            $key = Get-ItemProperty -Path $path -Name AGRESSO_REPORT
            $agresso_report_x86 = $key.AGRESSO_REPORT
            $key = Get-ItemProperty -Path $path -Name AGRESSO_CUSTOM
            $agresso_custom_x86 = $key.AGRESSO_CUSTOM
            $key = Get-ItemProperty -Path $path -Name AGRESSO_STYLESHEET
            $agresso_stylesheets_x86 = $key.AGRESSO_STYLESHEET
        }

        $path = "HKLM:\SOFTWARE\UNIT4\Business World " + $milestone + ".0.0\Data Sources\" + $ubwDsn + "\Business Server Environment"
        if ($milestone -lt 6)
        {
            $path = "HKLM:\SOFTWARE\Wow6432node\UNIT4\Agresso " + $milestone + "\Data Sources\" + $ubwDsn + "\Business Server Environment"
        }

        if (Test-Path $path)
        {
            $key = Get-ItemProperty -Path $path -Name AGRESSO_EXE
            $agresso_exe_x64 = $key.AGRESSO_EXE
            $key = Get-ItemProperty -Path $path -Name AGRESSO_IMPORT
            $agresso_import_x64 = $key.AGRESSO_IMPORT
            $key = Get-ItemProperty -Path $path -Name AGRESSO_REPORT
            $agresso_report_x64 = $key.AGRESSO_REPORT
            $key = Get-ItemProperty -Path $path -Name AGRESSO_CUSTOM
            $agresso_custom_x64 = $key.AGRESSO_CUSTOM
            $key = Get-ItemProperty -Path $path -Name AGRESSO_STYLESHEET
            $agresso_stylesheets_x64 = $key.AGRESSO_STYLESHEET
        }
        
        $path = "HKLM:\SOFTWARE\UNIT4\Business World " + $milestone + ".0.0"
        if ($milestone -lt 6)
        {
            $path = "HKLM:\SOFTWARE\Wow6432node\UNIT4\Agresso " + $milestone
        }
        
        # Web Services
        [string]$argWebSvcPath_ = [string]::Empty
        
        if (Test-Path ($root + "\Web Services Bin\") )
        {
            RetrieveWebServiceBin -argWebSvcPath ([ref]$argWebSvcPath_)
        }
        
        $agresso_webservices_x64 = $argWebSvcPath_

        # Web Api
        [string]$argWebBinApiPath_ = [string]::Empty
        
        if (Test-Path ($root + "\Web Api Bin\") )
        {
            RetrieveWebApiBin -argWebBinApiPath ([ref]$argWebBinApiPath_)
        }
        
        $agresso_webapi_x64 = $argWebBinApiPath_
        
        # Web
        [string]$argWebBinPath_ = [string]::Empty
        [string]$argWebCustomPath_ = [string]::Empty
        
        if (Test-Path ($root + "\Web\") )
        {
            RetrieveWebBinAndCustom -argWebBinPath ([ref]$argWebBinPath_) -argWebCustomPath ([ref]$argWebCustomPath_)
        }
        
        $agresso_web_bin_x64 = $argWebBinPath_
        $agresso_web_custom_x64 = $argWebCustomPath_

        if ($agresso_exe_x86 -ne $null)
        {
            WriteToOutput(("AGRESSO_EXE (x86)   : " + $agresso_exe_x86) *>&1)
            WriteToOutput(("AGRESSO_IMPORT (x86): " + $agresso_import_x86) *>&1)
            WriteToOutput(("AGRESSO_REPORT (x86): " + $agresso_report_x86) *>&1)
            WriteToOutput(("AGRESSO_CUSTOM (x86): " + $agresso_custom_x86) *>&1)
            WriteToOutput(("AGRESSO_STYLESHEET (x86): " + $agresso_stylesheets_x86) *>&1)
        }

        if ($agresso_exe_x64 -ne $null)
        {
            WriteToOutput(("AGRESSO_EXE (x64)   : " + $agresso_exe_x64) *>&1)
            WriteToOutput(("AGRESSO_IMPORT (x64): " + $agresso_import_x64) *>&1)
            WriteToOutput(("AGRESSO_REPORT (x64): " + $agresso_report_x64) *>&1)
            WriteToOutput(("AGRESSO_CUSTOM (x64): " + $agresso_custom_x64) *>&1)
            WriteToOutput(("AGRESSO_STYLESHEET (x64): " + $agresso_stylesheets_x64) *>&1)
            WriteToOutput(("AGRESSO_WEBSERVICES (x64): " + $agresso_webservices_x64) *>&1)
            WriteToOutput(("AGRESSO_WEBAPI (x64): " + $agresso_webapi_x64) *>&1)
            WriteToOutput(("AGRESSO_WEB (x64): " + $agresso_web_bin_x64) *>&1)
            WriteToOutput(("AGRESSO_WEB_CUSTOM (x64): " + $agresso_web_custom_x64) *>&1)
        }

        if (($agresso_exe_x86 -eq $null) -and ($agresso_exe_x64 -eq $null))
        {
            throw ("Failed to find registry entries for datasource " + $ubwDsn + ".")
        }       
    }
    else
    {
        # if dsn is not defined, we may still need copy files, so we need to know 
    
        # Find Agresso environment variables.
        $path = "HKLM:\SOFTWARE\Wow6432node\UNIT4\Business World " + $milestone + ".0.0\Data Sources\*\Business Server Environment"
        if (Test-Path $path)
        {
            $key = Get-ItemProperty -Path $path -Name AGRESSO_EXE
            $agresso_exe_x86 = $key.AGRESSO_EXE
            $key = Get-ItemProperty -Path $path -Name AGRESSO_IMPORT
            $agresso_import_x86 = $key.AGRESSO_IMPORT
            $key = Get-ItemProperty -Path $path -Name AGRESSO_REPORT
            $agresso_report_x86 = $key.AGRESSO_REPORT
            $key = Get-ItemProperty -Path $path -Name AGRESSO_CUSTOM
            $agresso_custom_x86 = $key.AGRESSO_CUSTOM
            $key = Get-ItemProperty -Path $path -Name AGRESSO_STYLESHEET
            $agresso_stylesheets_x86 = $key.AGRESSO_STYLESHEET
        }

        $path = "HKLM:\SOFTWARE\UNIT4\Business World " + $milestone + ".0.0\Data Sources\*\Business Server Environment"
        if ($milestone -lt 6)
        {
            $path = "HKLM:\SOFTWARE\Wow6432node\UNIT4\Agresso "+ $milestone + "\Data Sources\" + $ubwDsn + "\Business Server Environment"
        }

        if (Test-Path $path)
        {
            $key = Get-ItemProperty -Path $path -Name AGRESSO_EXE
            $agresso_exe_x64 = $key.AGRESSO_EXE
            $key = Get-ItemProperty -Path $path -Name AGRESSO_IMPORT
            $agresso_import_x64 = $key.AGRESSO_IMPORT
            $key = Get-ItemProperty -Path $path -Name AGRESSO_REPORT
            $agresso_report_x64 = $key.AGRESSO_REPORT
            $key = Get-ItemProperty -Path $path -Name AGRESSO_CUSTOM
            $agresso_custom_x64 = $key.AGRESSO_CUSTOM
            $key = Get-ItemProperty -Path $path -Name AGRESSO_STYLESHEET
            $agresso_stylesheets_x64 = $key.AGRESSO_STYLESHEET
        }
        
        $path = "HKLM:\SOFTWARE\UNIT4\Business World " + $milestone + ".0.0"
        if ($milestone -lt 6)
        {
            $path = "HKLM:\SOFTWARE\Wow6432node\UNIT4\Agresso " + $milestone
        }
        
        # Web Services
        [string]$argWebSvcPath_ = [string]::Empty
        
        if (Test-Path ($root + "\Web Services Bin\") )
        { 
            RetrieveWebServiceBin -argWebSvcPath ([ref]$argWebSvcPath_)
        }
        
        $agresso_webservices_x64 = $argWebSvcPath_

        # Web Api
        [string]$argWebBinApiPath_ = [string]::Empty
        
        if (Test-Path ($root + "\Web Api Bin\") )
        {
            RetrieveWebApiBin -argWebBinApiPath ([ref]$argWebBinApiPath_)
        }
        
        $agresso_webapi_x64 = $argWebBinApiPath_
        
        # Web
        [string]$argWebBinPath_ = [string]::Empty
        [string]$argWebCustomPath_ = [string]::Empty
        
        if (Test-Path ($root + "\Web\") )
        {
            RetrieveWebBinAndCustom -argWebBinPath ([ref]$argWebBinPath_) -argWebCustomPath ([ref]$argWebCustomPath_)
        }
        
        $agresso_web_bin_x64 = $argWebBinPath_
        $agresso_web_custom_x64 = $argWebCustomPath_

        if ($agresso_exe_x86 -ne $null)
        {
            WriteToOutput(("AGRESSO_EXE (x86)   : " + $agresso_exe_x86) *>&1)
            WriteToOutput(("AGRESSO_IMPORT (x86): " + $agresso_import_x86) *>&1)
            WriteToOutput(("AGRESSO_REPORT (x86): " + $agresso_report_x86) *>&1)
            WriteToOutput(("AGRESSO_CUSTOM (x86): " + $agresso_custom_x86) *>&1)
            WriteToOutput(("AGRESSO_STYLESHEET (x86): " + $agresso_stylesheets_x86) *>&1)
        }

        if ($agresso_exe_x64 -ne $null)
        {
            WriteToOutput(("AGRESSO_EXE (x64)   : " + $agresso_exe_x64) *>&1)
            WriteToOutput(("AGRESSO_IMPORT (x64): " + $agresso_import_x64) *>&1)
            WriteToOutput(("AGRESSO_REPORT (x64): " + $agresso_report_x64) *>&1)
            WriteToOutput(("AGRESSO_CUSTOM (x64): " + $agresso_custom_x64) *>&1)
            WriteToOutput(("AGRESSO_STYLESHEET (x64): " + $agresso_stylesheets_x64) *>&1)
            WriteToOutput(("AGRESSO_WEBSERVICES (x64): " + $agresso_webservices_x64) *>&1)
            WriteToOutput(("AGRESSO_WEBAPI (x64): " + $agresso_webapi_x64) *>&1)
            WriteToOutput(("AGRESSO_WEB (x64): " + $agresso_web_bin_x64) *>&1)
            WriteToOutput(("AGRESSO_WEB_CUSTOM (x64): " + $agresso_web_custom_x64) *>&1)
        }
    }
    

    cd ($root)
    if (Test-Path ".\Data Import\*")
    {
        cd ($root + "\Data Import")
        if ($agresso_import_x86 -ne $null)
        {
            WriteToOutput("Copy Data Import files (x86)")
            CopyFiles -Extension "*.*" -TargetPath "$agresso_import_x86" -File $logFile
        }
        if (($agresso_import_x64 -ne $null) -and ($agresso_import_x64 -ne $agresso_import_x86))
        {
            WriteToOutput("Copy Data Import files (x64)")
            CopyFiles -Extension "*.*" -TargetPath "$agresso_import_x64" -File $logFile
        }
    }

    cd ($root)
    if (Test-Path ".\Customised Reports\*")
    {
        cd ($root + "\Customised Reports")
        if ($agresso_custom_x86 -ne $null)
        {
            WriteToOutput("Copy Customised Reports files (x86)")
            CopyFiles -Extension "*.*" -TargetPath "$agresso_custom_x86" -File $logFile
        }
        if (($agresso_custom_x64 -ne $null) -and ($agresso_custom_x64 -ne $agresso_custom_x86))
        {
            WriteToOutput("Copy Customised Reports files (x64)")
            CopyFiles -Extension "*.*" -TargetPath "$agresso_custom_x64" -File $logFile
        }
    }
    
    cd ($root)
    if (Test-Path ".\Stylesheets\*")
    {
        cd ($root + "\Stylesheets")
        if ($agresso_stylesheets_x86 -ne $null)
        {
            WriteToOutput("Copy Stylesheets files (x86)")
            CopyFiles -Extension "*.*" -TargetPath "$agresso_stylesheets_x86" -File $logFile
        }
        if (($agresso_stylesheets_x64 -ne $null) -and ($agresso_stylesheets_x64 -ne $agresso_stylesheets_x86))
        {
            WriteToOutput("Copy Stylesheets files (x64)")
            CopyFiles -Extension "*.*" -TargetPath "$agresso_stylesheets_x64" -File $logFile
        }
    }
    
    cd ($root)
    if (Test-Path ".\Web Services Bin\*")
    {
        cd ($root + "\Web Services Bin")
        if ($agresso_webservices_x64 -ne $null)
        {
            WriteToOutput("Copy Web Services\Bin files (x64)")
            CopyFiles -Extension "*.*" -TargetPath "$agresso_webservices_x64" -File $logFile
        }
    }

    cd ($root)
    if (Test-Path ".\Web Api Bin\*")
    {
        cd ($root + "\Web Api Bin")
        if ($agresso_webapi_x64 -ne $null)
        {
            WriteToOutput("Copy Web Api\Bin files (x64)")
            CopyFiles -Extension "*.*" -TargetPath "$agresso_webapi_x64" -File $logFile
        }
    }
    
    cd ($root)
    if (Test-Path ".\Web\bin\*")
    {
        cd ($root + "\Web\bin")
        if ($agresso_web_bin_x64 -ne $null)
        {
            WriteToOutput("Copy Web\bin folder and files (x64)")
            Copy-Item -Path ".\*" -Filter "*.*" -Recurse -Force -Destination "$agresso_web_bin_x64" -Container -ErrorAction Stop *>&1 | Tee-Object -FilePath $logFile -Append
        }
    }
    
    cd ($root)
    if (Test-Path ".\Web\custom\*")
    {
        cd ($root + "\Web\custom")
        if ($agresso_web_custom_x64 -ne $null)
        {
            WriteToOutput("Copy Web\custom folder and files (x64)")
            Copy-Item -Path ".\*" -Filter "*.*" -Recurse -Force -Destination "$agresso_web_custom_x64" -Container -ErrorAction Stop *>&1 | Tee-Object -FilePath $logFile -Append
        }
    }

    cd ($root)
    if ($copyToBin -and (Test-Path ".\ACT\*"))
    {
        cd ($root + "\ACT")
        if ($agresso_exe_x86 -ne $null)
        {
            WriteToOutput("Copy .dll files to bin (x86)")
            CopyFiles -Extension "*.dll" -TargetPath "$agresso_exe_x86" -File $logFile
        }
        elseif ($agresso_exe_x64 -ne $null)
        {
            WriteToOutput("Copy .dll files to bin (x64)")
            CopyFiles -Extension "*.dll" -TargetPath "$agresso_exe_x64" -File $logFile
        }
    }


    <# **********************
    * Import ACT Assemblies *
    ************************* #>
    # Parameter "/s:" in actsetup:
    # N: Normal load
    # C: Closed
    # F: Always file
    # L: Always database
    # D: Deploy (database to file)
    # S: Secure load
    cd ($root)
    if (Test-Path ".\ACT\*")
    {
        try
        {
            WriteToOutput("Import ACT assemblies")
            
            cd ($root + "\ACT")
            if ($agresso_exe_x86 -ne $null)
            {
                ImportAct -argAgressoExe $agresso_exe_x86 -argUbwDsn $ubwDsn -argAgressoCustom $agresso_custom_x86 `
                    -argActStat $actStat -argActKeep $actKeep -argLogFile $logFile
            }
            elseif ($agresso_exe_x64 -ne $null)
            {
                ImportAct -argAgressoExe $agresso_exe_x64 -argUbwDsn $ubwDsn -argAgressoCustom $agresso_custom_x64 `
                    -argActStat $actStat -argActKeep $actKeep -argLogFile $logFile
            }
            if ($LastExitCode -ne 0) {
                throw "Failed to import ACT assemblies."
            }
        }
        catch
        {
            if($global:innerError -eq 1)
            {
                throw [System.Exception]
            }
            else
            {
                Write-Warning $Error[0] | Tee-Object -FilePath $logFile -Append
                $exitApp = Read-Host "Close Installer? [$($defaultValue)]"
                $exitApp = ($defaultValue,$exitApp)[[bool]$exitApp]
                
                if ($exitApp.ToString().ToUpper() -eq 'Y')
                {
                    $global:innerError = 1
                    throw [System.Exception]
                }
            }
        }
    }

    # Import TopGen screens.
    cd ($root)
    if ((Test-Path ".\TopGen\*") -and $database)
    {
        cd ($root + "\TopGen")
        if ($agresso_exe_x86 -ne $null)
        {
            WriteToOutput("Import TopGen screens.")
            & ($agresso_exe_x86 + "tgsetup.exe") /D:$ubwDsn /m ".\*.atg" | Tee-Object -FilePath $logFile -Append
        }
        elseif ($agresso_exe_x64 -ne $null)
        {
            WriteToOutput("Import TopGen screens.")
            & ($agresso_exe_x64 + "tgsetup.exe") /D:$ubwDsn /m ".\*.atg" | Tee-Object -FilePath $logFile -Append
        }
        if ($LastExitCode -ne 0)
        {
            throw "Failed to import TopGen screens."
        }
    }

    # Run ASQL scripts.
    cd ($root)
    $installpath = $root + "\Database Scripts"
    if ($database -and (Test-Path ($installpath + "\asql.lst")) -and ((Get-Content ($installpath + "\asql.lst")) -ne $null))
    {
        WriteToOutput("Database Scripts")

        $agressoClientDefaultValue = '*'
        
        if ([string]::IsNullOrEmpty($client))
        {               
            $agressoclient = Read-Host "Enter the UBW Client [$agressoClientDefaultValue]"          
        }
        else
        {       
            $agressoclient = $client.Trim()         
        }
        
        WriteToOutput("client: " + $client)
            
        if ([string]::IsNullOrEmpty($agressoclient))
        {
            $agressoclient = $agressoClientDefaultValue
        }

        $agressoclient = "SELECT '" + $agressoclient + "' INTO :vcClient"

        foreach($row in Get-Content ($installpath + "\asql.lst")) {
        
            if ($row -eq "")
            {
                continue;
            }
            
            WriteToOutput("Writing to ASQ")
            WriteToOutput("Row: $row")
            
            (Get-Content $installpath\$row).replace("SELECT 'agressoclienttoreplace' INTO :vcClient", $agressoclient) | Set-Content $installpath\$row
            
            WriteToOutput("Changed UBW Client")
        }

        WriteToOutput("Run ASQL.")

        cd ($installpath)
        if ($agresso_exe_x86 -ne $null)
        {
            & ($agresso_exe_x86 + "ASQL.exe") -D"$ubwDsn" -x
        }
        elseif ($agresso_exe_x64 -ne $null)
        {
            & ($agresso_exe_x64 + "ASQL.exe") -D"$ubwDsn" -x
        }
        if ($LastExitCode -ne 0)
        {
            throw "Failed to run ASQL scripts."
        }

        WriteToOutput("Run ASQL scripts.")

        cd ($installpath)
        foreach($row in Get-Content ($installpath + "\asql.lst")) {
        
            if ($row -eq "")
            {
                continue;
            }
        
            WriteToOutput("Writing to ASQ")
            WriteToOutput("Row: $row")

            (Get-Content $installpath\$row).replace($agressoclient, "SELECT 'agressoclienttoreplace' INTO :vcClient") | Set-Content $installpath\$row
            
            if ($agressofunctionchanged -ne "")
            {
                (Get-Content $installpath\$row).replace($agressofunctionchanged, $agressofunctionoriginal) | Set-Content $installpath\$row
            }

            if ($agressoassemblychanged -ne "")
            {
                (Get-Content $installpath\$row).replace($agressoassemblychanged, $agressoassemblyoriginal) | Set-Content $installpath\$row
            }

            if ($agressomenutextchanged -ne "")
            {
                (Get-Content $installpath\$row).replace($agressomenutextchanged, $agressomenutextoriginal) | Set-Content $installpath\$row
            }

            WriteToOutput("Changed UBW Client")
        }
    }

    if ($iisreset -eq $true)
    {
        # do an iisreset
        # do we have iis?
        
        if (get-service w3svc)
        {
            ## Get the ID and security principal of the current user account
            $myWindowsID = [System.Security.Principal.WindowsIdentity]::GetCurrent();
            $myWindowsPrincipal = New-Object System.Security.Principal.WindowsPrincipal($myWindowsID);

            ## Get the security principal for the administrator role
            $adminRole = [System.Security.Principal.WindowsBuiltInRole]::Administrator;
            ## Check to see if we are currently running as an administrator
            if (-not $myWindowsPrincipal.IsInRole($adminRole))
            {
                # We are not running as an administrator, so launch iisreset as administrator
                Start-Process PowerShell.exe -ArgumentList iisreset -Wait -Verb RunAs
                WriteToOutput("IISReset, trying to run as Administrator")
            }
            else
            {
                iisreset | Tee-Object -FilePath $logFile -Append
                WriteToOutput("IISReset, run as Administrator")
            }
        }
    }
}
catch
{
    ## only prints the error message if it's not an inner exception
    if ($global:innerError -ne 1)
    {
        Write-Warning $Error[0] | Tee-Object -FilePath $logFile -Append
    }
    cd ($curr)
    Exit
}

cd ($curr)
WriteToOutput("Install completed successfully.")
