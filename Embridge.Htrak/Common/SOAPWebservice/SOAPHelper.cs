﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Embridge.Htrak.Common.SOAPWebservice
{
    public static class SOAPHelper
    {
        public static string ImportWSAction { get => "http://services.agresso.com/ImportService/ImportV200606/ExecuteServerProcessAsynchronously"; }

        public static HttpWebRequest CreateSOAPWebRequest(string url, string action)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Headers.Add($"SOAPAction:{action}");
            httpWebRequest.ContentType = "text/xml;charset=\"utf-8\"";
            httpWebRequest.Accept = "text/xml";
            httpWebRequest.Method = "POST";

            return httpWebRequest;
        }

        public static HttpClient CreateSOAPHttpClient(string url, string action)
        {
            var handler = new WebRequestHandler();

            if (ProtocolIsHTTPS(url))
                handler.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

            var MyHttpClient = new HttpClient(handler);
            MyHttpClient.DefaultRequestHeaders.Add("SOAPAction", action);

            return MyHttpClient;
        }

        private static bool ProtocolIsHTTPS(string uri)
        {
            return uri.ToLower().StartsWith("https");
        }
    }
}
