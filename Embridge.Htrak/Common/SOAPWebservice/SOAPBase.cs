﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using Agresso.Interface.CommonExtension;

namespace Embridge.Htrak.Common.SOAPWebservice
{
    public class SOAPBase
    {
        public string SOAPUrl { get; private set; }
        public string SOAPAction { get; private set; }
        private string SOAPUsername { get; set; }
        private string SOAPPassword { get; set; }
        private string SOAPClient { get; set; }
        public bool SOAPCallSuccess { get; private set; }
        public string SOAPResponse { get; private set; }
        protected HttpClient SOAPHttpClient { get; private set; }
        public int OrderNo { get; private set; }
        public bool SchemaValidated { get; private set; }
        public IList<string> SchemaValidationErrors { get; private set; } 


        public SOAPBase(string username, string password, string client)
        {
            SOAPUsername = username;
            SOAPPassword = password;
            SOAPClient = client;
            SchemaValidated = true;
            SchemaValidationErrors = new List<string>();
        }

        protected void CreateWebRequest(string url, string action)
        {
            if (string.IsNullOrWhiteSpace(url) || string.IsNullOrWhiteSpace(action))
                return;

            SOAPUrl = url;
            SOAPAction = action;
            SOAPHttpClient = SOAPHelper.CreateSOAPHttpClient(SOAPUrl, SOAPAction);
        }

        protected bool MakeSOAPCall(string soapPayload)
        {
            if (SOAPHttpClient == default)
            {
                SOAPCallSuccess = false;
                SOAPResponse = "No HttpWebRequest";
                return SOAPCallSuccess;
            }

            try
            {
                //XmlDocument SOAPReqBody = new XmlDocument();
                //SOAPReqBody.LoadXml(soapPayload);

                CurrentContext.Message.Display($"Making HttpClient call to: {SOAPUrl}");

                var httpContent = new StringContent(soapPayload, Encoding.UTF8, "text/xml");

                var httpClientResponse = SOAPHttpClient.PostAsync(SOAPUrl, httpContent).Result;

                CurrentContext.Message.Display("Called HttpClient");

                //using (Stream stream = SOAPHttpWebRequest.GetRequestStream())
                //{
                //    SOAPReqBody.Save(stream);
                //}

                //using (WebResponse Service = SOAPHttpWebRequest.GetResponse())
                //{
                //    using (StreamReader rd = new StreamReader(Service.GetResponseStream()))
                //    {
                //        SOAPResponse = rd.ReadToEnd();
                //    }
                //}

                SOAPCallSuccess = httpClientResponse.IsSuccessStatusCode;
                CurrentContext.Message.Display($"Webservice Response Code: {httpClientResponse.StatusCode.ToString()}");
                SOAPResponse = httpClientResponse.Content.ReadAsStringAsync().Result;

                return SOAPCallSuccess;
            }
            catch (Exception e)
            {
                CurrentContext.Message.Display($"Exception Calling Webservice");
                SOAPCallSuccess = false;
                SOAPResponse = e.Message;
                return SOAPCallSuccess;
            }
        }

        protected string CreateImportWSXML(string cdataTag, string processId, string menuId, int variantId)
        {
            var xmlPayload = $"<![CDATA[{cdataTag.Replace("\n", "").Replace("\r", "")}]]>";

            var soapPayload = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:imp=""http://services.agresso.com/ImportService/ImportV200606""> " +
                $"<soapenv:Header/>" +
                $"<soapenv:Body>" +
                $"<imp:ExecuteServerProcessAsynchronously>" +
                $"<imp:input>" +
                $"<imp:ServerProcessId>{processId}</imp:ServerProcessId>" +
                $"<imp:MenuId>{menuId}</imp:MenuId>" +
                $"<imp:Variant>{variantId}</imp:Variant>" +
                $"<imp:Xml>{xmlPayload}</imp:Xml>" +
                $"</imp:input>" +
                $"<imp:credentials>" +
                $"<imp:Username>{SOAPUsername}</imp:Username>" +
                $"<imp:Client>{SOAPClient}</imp:Client>" +
                $"<imp:Password>{SOAPPassword}</imp:Password>" +
                $"</imp:credentials>" +
                $"</imp:ExecuteServerProcessAsynchronously>" +
                $"</soapenv:Body>" +
                $"</soapenv:Envelope>";

            return soapPayload;
        }

        protected void GetOrderNumberFromResponse()
        {
            OrderNo = 0;

            if (string.IsNullOrWhiteSpace(SOAPResponse))
                OrderNo = 0;

            string patternMatch = @"<OrderNumber>(.*?)<\/OrderNumber>";

            var matches = Regex.Matches(SOAPResponse, patternMatch);

            if (matches.Count != 1)
                OrderNo = 0;

            if (!matches[0].Success)
                OrderNo = 0;

            if (int.TryParse(matches[0].Groups[1].Value, out int foundOrderNo))
                OrderNo = foundOrderNo;
        }

        public bool ValidateImportXML(string importXMLToValidate, string nameSpace, string schema, out string message, bool debug = false)
        {
            if(debug)
            {
                CurrentContext.Message.Display($"NameSpace: {nameSpace}");
                CurrentContext.Message.Display("****************************************************");
                CurrentContext.Message.Display($"Schema:");
                CurrentContext.Message.Display(schema);
                CurrentContext.Message.Display("****************************************************");
                CurrentContext.Message.Display("XML to validate:");
                CurrentContext.Message.Display(importXMLToValidate);
                CurrentContext.Message.Display("****************************************************");
            }

            try
            {
                XmlReader reader = XmlReader.Create(new StringReader(schema));
                XmlReaderSettings settings = new XmlReaderSettings();

                if(debug)
                    CurrentContext.Message.Display($"XMLReader and XMLSettings Created");

                settings.Schemas.Add(nameSpace, reader);
                settings.ValidationType = ValidationType.Schema;
                settings.ValidationEventHandler += Settings_ValidationEventHandler;

                if (debug)
                    CurrentContext.Message.Display($"Added details to settings");

                XmlReader reader2 = XmlReader.Create(new StringReader(importXMLToValidate), settings);

                if (debug)
                    CurrentContext.Message.Display($"Created Reader");

                XmlDocument document = new XmlDocument();
                document.Load(reader2);

                if (debug)
                    CurrentContext.Message.Display($"Created XMLDocument");

                if (!SchemaValidated)
                    message = "Import XML did not validate against schema";

                message = "Validation okay";
            }
            catch (Exception e)
            {
                message = $"Validating Import XML: {e.Message}";
                SchemaValidationErrors.Add(message);
                SchemaValidated = false;
            }

            return SchemaValidated;
        }

        private void Settings_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    SchemaValidated = false;
                    SchemaValidationErrors.Add($"Error: {e.Message}");
                    break;
                case XmlSeverityType.Warning:
                    SchemaValidationErrors.Add($"Warning: {e.Message}");
                    break;
            }
        }
    }
}
