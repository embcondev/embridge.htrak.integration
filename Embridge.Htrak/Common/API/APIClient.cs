﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Embridge.Htrak.Common.API
{
    public static class APIClient
    {
        public static HttpClient GetHttpClient(string username, string password, int timeout, string uri)
        {
            var handler = new WebRequestHandler();

            if (ProtocolIsHTTPS(uri))
                handler.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

            var MyHttpClient = new HttpClient(handler);
            MyHttpClient.Timeout = TimeSpan.FromSeconds(timeout);
            if(!string.IsNullOrWhiteSpace(username) && !string.IsNullOrWhiteSpace(password))
                MyHttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", EncodeTo64($"{username}:{password}"));
            return MyHttpClient;
        }

        private static string EncodeTo64(string s)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(s));
        }

        private static bool ProtocolIsHTTPS(string uri)
        {
            return uri.ToLower().StartsWith("https");
        }
    }
}
