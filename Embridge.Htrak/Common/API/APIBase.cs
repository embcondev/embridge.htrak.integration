﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Embridge.Htrak.Common.API
{
    public class APIBase
    {
        public string ApiUrl { get; private set; }
        private string ApiUsername { get; set; }
        private string ApiPassword { get; set; }
        public bool APICallSuccess { get; private set; }
        public string APIResponse { get; private set; }
        public string APIResponseCode { get; private set; }
        public string APIPOSTContent { get; private set; }
        protected HttpClient APIHttpClient { get; private set; }

        public APIBase(string username, string password)
        {
            ApiUsername = username;
            ApiPassword = password;
        }

        protected void AddHeader(string name, string value)
        {
            if(APIHttpClient != default)
                APIHttpClient.DefaultRequestHeaders.Add(name, value);
        }

        protected void CreateHttpClient(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
                return;

            ApiUrl = url;
            APIHttpClient = APIClient.GetHttpClient(ApiUsername, ApiPassword, 100, ApiUrl);
        }

        protected bool MakeAPIGetCall()
        {
            if(APIHttpClient == default)
            {
                APICallSuccess = false;
                APIResponse = "No HttpClient";
                APIResponseCode = "500";
                return APICallSuccess;
            }

            var httpClientResponse = APIHttpClient.GetAsync(ApiUrl).Result;
            var statusCode = (int)httpClientResponse.StatusCode;


            APICallSuccess = httpClientResponse.IsSuccessStatusCode;
            APIResponseCode = statusCode.ToString();
            APIResponse = httpClientResponse.Content.ReadAsStringAsync().Result;

            //Test Data for PO
            //APIResponse = @"<agr:ABWOrder xmlns:agrlib=""http://services.agresso.com/schema/ABWSchemaLib/2011/11/14"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:agr=""http://services.agresso.com/schema/ABWOrder/2011/11/14""><agr:Order><agrlib:OrderNo>10001</agrlib:OrderNo><agrlib:VoucherType>IO</agrlib:VoucherType><agr:TransType>41</agr:TransType><agrlib:Period>202106</agrlib:Period><agr:Header><agr:OrderDate>2021-06-08</agr:OrderDate><agr:ExtOrderRef>7DF16F4A-E993-4063-9CDE-E286208DDE73</agr:ExtOrderRef><agr:ExtOrderId>HT0003</agr:ExtOrderId><agr:ObsDate>2021-06-08</agr:ObsDate><agrlib:Text2 /><agrlib:Currency>GBP</agrlib:Currency><agr:ExchRate>1</agr:ExchRate><agrlib:PayTerms>03</agrlib:PayTerms><agrlib:PayMethod>IP</agrlib:PayMethod><agrlib:TemplateId>0</agrlib:TemplateId><agrlib:VoucherRef>0</agrlib:VoucherRef><agr:Seller><agrlib:SellerNo>1000</agrlib:SellerNo></agr:Seller><agr:Buyer><agr:BuyerReferences><agr:Responsible></agr:Responsible><agr:RequestedBy>ALBERT</agr:RequestedBy><agr:Accountable></agr:Accountable></agr:BuyerReferences></agr:Buyer><agrlib:Bespoke><agrlib:FreeText /></agrlib:Bespoke></agr:Header><agr:Details><agr:Detail><agr:LineNo>1</agr:LineNo><agr:BuyerProductCode>1000</agr:BuyerProductCode><agr:UnitCode>UN</agr:UnitCode><agr:Quantity>1</agr:Quantity><agr:Price>1000.00</agr:Price><agrlib:DiscountPct>0</agrlib:DiscountPct><agrlib:Discount>0.00</agrlib:Discount><agr:LineTotal>1000.00</agr:LineTotal><agr:DetailInfo><agrlib:UseLineTotal>false</agrlib:UseLineTotal><agrlib:AllocationKey>0</agrlib:AllocationKey><agrlib:TemplateId>0</agrlib:TemplateId></agr:DetailInfo><agrlib:ProductSpecification><agrlib:SeqNo>1</agrlib:SeqNo><agrlib:Info>Lot No.: A2321333</agrlib:Info></agrlib:ProductSpecification><agr:Delivery><agrlib:DelivDate>2021-06-09</agrlib:DelivDate></agr:Delivery></agr:Detail><agr:Detail><agr:LineNo>2</agr:LineNo><agr:BuyerProductCode>1001</agr:BuyerProductCode><agr:UnitCode>UN</agr:UnitCode><agr:Quantity>2</agr:Quantity><agr:Price>750.00</agr:Price><agrlib:DiscountPct>0</agrlib:DiscountPct><agrlib:Discount>0.00</agrlib:Discount><agr:LineTotal>1500.00</agr:LineTotal><agr:DetailInfo><agrlib:UseLineTotal>false</agrlib:UseLineTotal><agrlib:AllocationKey>0</agrlib:AllocationKey><agrlib:TemplateId>0</agrlib:TemplateId></agr:DetailInfo><agrlib:ProductSpecification><agrlib:SeqNo>1</agrlib:SeqNo><agrlib:Info>Lot No.: C34343</agrlib:Info></agrlib:ProductSpecification><agrlib:ProductSpecification><agrlib:SeqNo>2</agrlib:SeqNo><agrlib:Info>Note: Test comment 1</agrlib:Info></agrlib:ProductSpecification><agr:Delivery><agrlib:DelivDate>2021-06-09</agrlib:DelivDate></agr:Delivery></agr:Detail><agr:Detail><agr:LineNo>3</agr:LineNo><agr:BuyerProductCode>1002</agr:BuyerProductCode><agr:UnitCode>UN</agr:UnitCode><agr:Quantity>1</agr:Quantity><agr:Price>750.00</agr:Price><agrlib:DiscountPct>0</agrlib:DiscountPct><agrlib:Discount>0.00</agrlib:Discount><agr:LineTotal>750.00</agr:LineTotal><agr:DetailInfo><agrlib:UseLineTotal>false</agrlib:UseLineTotal><agrlib:AllocationKey>0</agrlib:AllocationKey><agrlib:TemplateId>0</agrlib:TemplateId></agr:DetailInfo><agrlib:ProductSpecification><agrlib:SeqNo>1</agrlib:SeqNo><agrlib:Info>Note: Test comment 2</agrlib:Info></agrlib:ProductSpecification><agr:Delivery><agrlib:DelivDate>2021-06-09</agrlib:DelivDate></agr:Delivery></agr:Detail></agr:Details><agr:Summary><agrlib:TotalExclTax>3250.00</agrlib:TotalExclTax><agrlib:TotalTax>0.00</agrlib:TotalTax><agrlib:OrderDiscPct>0</agrlib:OrderDiscPct><agrlib:OrderDiscAmount>0.00</agrlib:OrderDiscAmount><agrlib:TotalInclTax>3250.00</agrlib:TotalInclTax></agr:Summary></agr:Order><agr:Order><agrlib:OrderNo>10002</agrlib:OrderNo><agrlib:VoucherType>X1</agrlib:VoucherType><agr:TransType>41</agr:TransType><agrlib:Period>202106</agrlib:Period><agr:Header><agr:OrderDate>2021-06-08</agr:OrderDate><agr:ExtOrderRef>7DF16F4A-E993-4063-9CDE-E286208DDE73</agr:ExtOrderRef><agr:ExtOrderId>HT0004</agr:ExtOrderId><agr:ObsDate>2021-06-08</agr:ObsDate><agrlib:Text2 /><agrlib:Currency>GBP</agrlib:Currency><agr:ExchRate>1</agr:ExchRate><agrlib:PayTerms>03</agrlib:PayTerms><agrlib:PayMethod>IP</agrlib:PayMethod><agrlib:TemplateId>0</agrlib:TemplateId><agrlib:VoucherRef>0</agrlib:VoucherRef><agr:Seller><agrlib:SellerNo>1000</agrlib:SellerNo></agr:Seller><agr:Buyer><agr:BuyerReferences><agr:Responsible></agr:Responsible><agr:RequestedBy>ALBERT</agr:RequestedBy><agr:Accountable></agr:Accountable></agr:BuyerReferences></agr:Buyer><agrlib:Bespoke><agrlib:FreeText /></agrlib:Bespoke></agr:Header><agr:Details><agr:Detail><agr:LineNo>1</agr:LineNo><agr:BuyerProductCode>1000</agr:BuyerProductCode><agr:UnitCode>UN</agr:UnitCode><agr:Quantity>2</agr:Quantity><agr:Price>1000.00</agr:Price><agrlib:DiscountPct>0</agrlib:DiscountPct><agrlib:Discount>0.00</agrlib:Discount><agr:LineTotal>2000.00</agr:LineTotal><agr:DetailInfo><agrlib:UseLineTotal>false</agrlib:UseLineTotal><agrlib:AllocationKey>0</agrlib:AllocationKey><agrlib:TemplateId>0</agrlib:TemplateId></agr:DetailInfo><agrlib:ProductSpecification><agrlib:SeqNo>1</agrlib:SeqNo><agrlib:Info>Lot No.: AA232</agrlib:Info></agrlib:ProductSpecification><agr:Delivery><agrlib:DelivDate>2021-06-09</agrlib:DelivDate></agr:Delivery></agr:Detail><agr:Detail><agr:LineNo>2</agr:LineNo><agr:BuyerProductCode>1001</agr:BuyerProductCode><agr:UnitCode>UN</agr:UnitCode><agr:Quantity>3</agr:Quantity><agr:Price>750.00</agr:Price><agrlib:DiscountPct>0</agrlib:DiscountPct><agrlib:Discount>0.00</agrlib:Discount><agr:LineTotal>2250.00</agr:LineTotal><agr:DetailInfo><agrlib:UseLineTotal>false</agrlib:UseLineTotal><agrlib:AllocationKey>0</agrlib:AllocationKey><agrlib:TemplateId>0</agrlib:TemplateId></agr:DetailInfo><agrlib:ProductSpecification><agrlib:SeqNo>1</agrlib:SeqNo><agrlib:Info>Lot No.: AA5444</agrlib:Info></agrlib:ProductSpecification><agr:Delivery><agrlib:DelivDate>2021-06-09</agrlib:DelivDate></agr:Delivery></agr:Detail></agr:Details><agr:Summary><agrlib:TotalExclTax>4250.00</agrlib:TotalExclTax><agrlib:TotalTax>0.00</agrlib:TotalTax><agrlib:OrderDiscPct>0</agrlib:OrderDiscPct><agrlib:OrderDiscAmount>0.00</agrlib:OrderDiscAmount><agrlib:TotalInclTax>4250.00</agrlib:TotalInclTax></agr:Summary></agr:Order></agr:ABWOrder>";
            //APIResponseCode = "200";

            return APICallSuccess;
        }

        protected bool MakeAPIPOSTCall(string jsonContent)
        {
            APIPOSTContent = jsonContent;

            if (APIHttpClient == default)
            {
                APICallSuccess = false;
                APIResponse = "No HttpClient";
                APIResponseCode = "500";
                return APICallSuccess;
            }

            var httpContent = new StringContent(APIPOSTContent, Encoding.UTF8, "application/json");

            var httpClientResponse = APIHttpClient.PostAsync(ApiUrl, httpContent).Result;

            APICallSuccess = httpClientResponse.IsSuccessStatusCode;
            APIResponseCode = httpClientResponse.StatusCode.ToString();
            APIResponse = httpClientResponse.Content.ReadAsStringAsync().Result;

            return APICallSuccess;
        }
    }
}
