﻿using System;
using System.Reflection;

namespace Embridge.Htrak.Common
{
    /// <summary>
    /// Information about the currently executing assembly.
    /// </summary>
    public class ThisAssembly
    {
        #region public properties
        public string Description { get; }
        public string Name { get; }
        public string Version { get; }
        #endregion

        #region Constructors
        /// <summary>
        /// Load assembly information.
        /// </summary>
        public ThisAssembly()
        {
            var MyAssembly = Assembly.GetExecutingAssembly();

            var MyAssemblyDescription = (AssemblyDescriptionAttribute)Attribute.GetCustomAttribute(MyAssembly, typeof(AssemblyDescriptionAttribute));

            if (MyAssemblyDescription == default(AssemblyDescriptionAttribute))
            {
                Description = "N/A";
            }
            else
            {
                Description = MyAssemblyDescription.Description;
            }

            var MyAssemblyName = MyAssembly.GetName();

            if (MyAssemblyName == default)
            {
                Name = "N/A";

                var MyAssemblyVersion = (AssemblyVersionAttribute)Attribute.GetCustomAttribute(MyAssembly, typeof(AssemblyVersionAttribute));

                if (MyAssemblyVersion == default(AssemblyVersionAttribute))
                {
                    Version = "N/A";
                }
                else
                {
                    Version = MyAssemblyVersion.Version;
                }
            }
            else
            {
                Name = MyAssemblyName.Name;

                Version = $"{MyAssembly.GetName().Version}";
            }
        }
        #endregion
    }
}
