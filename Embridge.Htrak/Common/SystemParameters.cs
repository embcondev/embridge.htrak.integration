﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Embridge.Htrak.Common
{
    public static class SystemParameters
    {
        /// <summary>
        /// Default period from.
        /// </summary>
        public const string DEF_PERIOD_FROM = "DEF_PERIOD_FROM";
        /// <summary>
        /// Default period to.
        /// </summary>
        public const string DEF_PERIOD_TO = "DEF_PERIOD_TO";
    }

    public static class SystemSetupValues
    {
        public const string EK_HTRAK_API = "EK_HTRAK_API";
        public const string EK_HTRAK_U4WS = "EK_HTRAK_U4WS";
    }
}
