﻿using Agresso.Interface.CommonExtension;
using Agresso.ServerExtension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Embridge.Htrak.Common.Server
{
    abstract internal class ParametersBase
    {
        #region Private properties
        public IReport Me { get; private set; }
        #endregion

        #region Standard parameter names
        public const string ClientCol = "client";
        public const string LanguageCol = "language";
        public const string OrdernoCol = "orderno";
        public const string UserIdCol = "user_id";
        #endregion

        #region Standard parameter values
        /// <summary>
        /// Client.
        /// </summary>
        public string Client => GetString(ClientCol);
        /// <summary>
        /// Language.
        /// </summary>
        public string Language => GetString(LanguageCol);
        /// <summary>
        /// Order number.
        /// </summary>
        public int Orderno => GetInt(OrdernoCol);
        /// <summary>
        /// User id.
        /// </summary>
        public string UserId => GetString(UserIdCol);
        #endregion

        #region Constructors
        public ParametersBase(IReport report) { Me = report; }
        #endregion

        #region Protected public =methods
        /// <summary>
        /// Return a string parameter value.
        /// </summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <param name="defaultValue">Default parameter value.</param>
        /// <returns>Parameter value.</returns>
        protected string GetString(string parameterName, string defaultValue = default)
        {
            var parameterValue = default(string);

            if (Me.API.GetParameterNames().Contains(parameterName))
                parameterValue = Me.API.GetParameter(parameterName);
            else if (defaultValue == default)
                throw new Exception("Parameter '" + parameterName + "' does not exist.");

            if (!string.IsNullOrWhiteSpace(parameterValue))
                return parameterValue;
            else if (defaultValue == default)
                throw new Exception("Parameter '" + parameterName + "' is blank.");

            return defaultValue;
        }

        /// <summary>
        /// Return a DateTime parameter value.
        /// </summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <param name="defaultValue">Default parameter value.</param>
        /// <returns>Parameter value.</returns>
        protected DateTime GetDateTime(string parameterName, DateTime? defaultValue = null)
        {
            if (Me.API.GetParameterNames().Contains(parameterName))
            {
                string parameterValue = GetString(parameterName);
                if (!IsValidDateTimeStringFormat(parameterValue))
                    throw new Exception("Parameter '" + parameterName + "' value '" + parameterValue + "' is not a valid Agresso date/time string.");
                return GetStringAsDateTime(parameterValue);
            }
            else
            {
                if (defaultValue == null)
                    throw new Exception("Parameter '" + parameterName + "' does not exist.");
                return (DateTime)defaultValue;
            }
        }

        /// <summary>
        /// Return a double parameter value.
        /// </summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <param name="defaultValue">Default parameter value.</param>
        /// <returns>Parameter value.</returns>
        protected double GetDouble(string parameterName, double? defaultValue = null)
        {
            if (Me.API.GetParameterNames().Contains(parameterName))
            {
                string parameterValue = GetString(parameterName);

                if (double.TryParse(parameterValue, out var result))
                {
                    return result;
                }
                else
                {
                    throw new Exception("Parameter '" + parameterName + "' value '" + parameterValue + "' is not a valid double.");
                }
            }
            else
            {
                if (defaultValue != null)
                {
                    return (double)defaultValue;
                }
                else
                {
                    throw new Exception("Parameter '" + parameterName + "' does not exist.");
                }
            }
        }

        /// <summary>
        /// Return an int parameter value.
        /// </summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <param name="defaultValue">Default parameter value.</param>
        /// <returns>Parameter value.</returns>
        protected int GetInt(string parameterName, int? defaultValue = null)
        {
            if (Me.API.GetParameterNames().Contains(parameterName))
            {
                string parameterValue = GetString(parameterName);
                if (!int.TryParse(parameterValue, out var result))
                    throw new Exception("Parameter '" + parameterName + "' value '" + parameterValue + "' is not a valid int.");
                return result;
            }
            else
            {
                if (defaultValue == null)
                    throw new Exception("Parameter '" + parameterName + "' does not exist.");
                return (int)defaultValue;
            }
        }

        /// <summary>
        /// Return an int parameter value.
        /// </summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <param name="defaultValue">Default parameter value.</param>
        /// <returns>Parameter value.</returns>
        protected long GetLong(string parameterName, long? defaultValue = null)
        {
            if (Me.API.GetParameterNames().Contains(parameterName))
            {
                string parameterValue = GetString(parameterName);
                if (!long.TryParse(parameterValue, out var result))
                    throw new Exception("Parameter '" + parameterName + "' value '" + parameterValue + "' is not a valid long.");
                return result;
            }
            else
            {
                if (defaultValue == null)
                    throw new Exception("Parameter '" + parameterName + "' does not exist.");
                return (long)defaultValue;
            }
        }

        /// <summary>
        /// Return a bool parameter value.
        /// </summary>
        /// <param name="parameterName">Parameter name.</param>
        /// <param name="defaultValue">Default parameter value.</param>
        /// <returns>Parameter value.</returns>
        protected bool GetBool(string parameterName, bool? defaultValue = null)
        {
            if (Me.API.GetParameterNames().Contains(parameterName))
            {
                string parameterValue = GetString(parameterName);
                if (!int.TryParse(parameterValue, out var result))
                    throw new Exception("Parameter '" + parameterName + "' value '" + parameterValue + "' is not a valid bool.");
                return result == 1;
            }
            else
            {
                if (defaultValue == null)
                    throw new Exception("Parameter '" + parameterName + "' does not exist.");
                return (bool)defaultValue;
            }
        }

        protected string GetandValidateSystemParameter(string systemParameterName)
        {
            var sysParamValue = string.Empty;
            Me.API.GetSystemParameter(systemParameterName, ref sysParamValue);

            if (!string.IsNullOrEmpty(sysParamValue))
                return sysParamValue;

            Me.API.WriteLog($"ERROR: {systemParameterName} not configured.");

            return string.Empty;
        }

        protected SysSetupValueList GetSysSetupValues(string name, string sysSetupCode)
        {
            var sysSetupValueList = new SysSetupValueList();

            var iSQL = CurrentContext.Database.CreateStatement();
            iSQL.UseAgrParser = true;
            iSQL.Assign("SELECT name, text1, number1, text2, number2, text3, number3, description, sequence_no");
            iSQL.Append(" FROM aagsysvalues");
            iSQL.Append(" WHERE name = @name");
            iSQL.Append(" AND sys_setup_code = @sysSetupCode");
            iSQL.Append(" ORDER BY sequence_no");
            iSQL.SetParameter("name", name);
            iSQL.SetParameter("sysSetupCode", sysSetupCode);

            try
            {
                var foundSysSetupValues = new DataTable();
                if (CurrentContext.Database.Read(iSQL, foundSysSetupValues) > 0)
                {
                    foreach (DataRow currentRow in foundSysSetupValues.Rows)
                    {
                        var sysSetupValue = new SysSetupValue(name: currentRow.Field<string>("name")
                            , text1: currentRow.Field<string>("text1")
                            , number1: currentRow.Field<int>("number1")
                            , text2: currentRow.Field<string>("text2")
                            , number2: currentRow.Field<int>("number2")
                            , text3: currentRow.Field<string>("text3")
                            , number3: currentRow.Field<int>("number3")
                            , description: currentRow.Field<string>("description")
                            , sequenceNo: currentRow.Field<int>("sequence_no"));

                        sysSetupValueList.SysSetupValues.Add(sysSetupValue);
                    }
                }
            }
            catch (Exception e)
            {
                Me.API.WriteLog($"ERROR: Getting System Setup Values for: {name}, {sysSetupCode}");
                Me.API.WriteLog($"ERROR: Message: {e.Message}");
            }

            return sysSetupValueList;
        }

        #endregion

        #region Static public methods
        /// <summary>
        /// Convert Agresso date/time string to a DateTime variable.
        /// </summary>
        static public DateTime GetStringAsDateTime(string date)
        {
            if (IsValidDateTimeStringFormat(date))
            {
                if (date.Length == 8)
                {
                    date += " 00:00:00";
                }

                return new DateTime(Convert.ToInt32(date.Substring(0, 4), CultureInfo.InvariantCulture),
                                    Convert.ToInt32(date.Substring(4, 2), CultureInfo.InvariantCulture),
                                    Convert.ToInt32(date.Substring(6, 2), CultureInfo.InvariantCulture),
                                    Convert.ToInt32(date.Substring(9, 2), CultureInfo.InvariantCulture),
                                    Convert.ToInt32(date.Substring(12, 2), CultureInfo.InvariantCulture),
                                    Convert.ToInt32(date.Substring(15, 2), CultureInfo.InvariantCulture));
            }
            else
            {
                throw new Exception("Value '" + date + "' is not a valid Agresso date/time string.");
            }
        }

        /// <summary>
        /// Convert DateTime variable to an Agresso date/time string.
        /// </summary>
        static public string GetDateTimeAsString(DateTime date)
            => $"{date:yyyyMMdd HH:mm:ss}";

        #endregion

        #region Static private methods
        /// <summary>
        /// Return true if string is a valid Agresso date/time string.
        /// </summary>
        static private bool IsValidDateTimeStringFormat(string date)
            => Regex.IsMatch(date, @"\d{8} \d{2}:\d{2}:\d{2}") || Regex.IsMatch(date, @"\d{8}");
        #endregion
    }

    public class SysSetupValueList
    {
        public SysSetupValueList()
        {
            SysSetupValues = new List<SysSetupValue>();
        }

        public IList<SysSetupValue> SysSetupValues { get; set; }
    }

    public class SysSetupValue
    {
        public SysSetupValue(string name, string text1 = default, int number1 = 0
            , string text2 = default, int number2 = 0
            , string text3 = default, int number3 = 0
            , string description = default, int sequenceNo = 0)
        {
            Name = name;
            Text1 = text1;
            Number1 = number1;
            Text2 = text2;
            Number2 = number2;
            Text3 = text3;
            Number3 = number3;
            Description = description;
            SequenceNo = sequenceNo;
        }

        public string Name { get; private set; }
        public string Text1 { get; private set; }
        public int Number1 { get; private set; }
        public string Text2 { get; private set; }
        public int Number2 { get; private set; }
        public string Text3 { get; private set; }
        public int Number3 { get; private set; }
        public string Description { get; private set; }
        public int SequenceNo { get; private set; }
    }
}
