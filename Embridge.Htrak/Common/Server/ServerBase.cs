﻿using Agresso.Interface.CommonExtension;
using Agresso.ServerExtension;
using System;
using System.Collections.Generic;
using System.Data;

namespace Embridge.Htrak.Common.Server
{
    public class ServerBase
    {
        #region Id
        private const string Id = "SRVBAS";
        #endregion

        #region public poperties
        public IReport Me { get; set; }
        #endregion

        public IStatement CreateIStatement(bool useAGRParser = true)
        {
            var iSQL = CurrentContext.Database.CreateStatement();
            iSQL.UseAgrParser = useAGRParser;
            return iSQL;
        }

        /// <summary>
        /// Write current assembly information to log file.
        /// </summary>
        public void WriteAssemblyInformation(bool Seperator = true)
        {
            var myAssembly = new ThisAssembly();

            if (Seperator) WriteSeparator();
            Me.API.WriteLog($"Assembly = {myAssembly.Name}, Version = {myAssembly.Version}");
            if (Seperator) WriteSeparator();

            Me.API.SetReportParameter(ServerBaseParameters.AssemblyVersionCol, myAssembly.Version);     // for displaying in report outputs
        }

        public void WriteWarningMessage(string message)
        {
            Me.API.WriteLog($"WARNING: {message}");
        }

        public void WriteErrorMessage(string message)
        {
            Me.API.WriteLog($"ERROR: {message}");
        }

        public void WriteMessage(string message)
        {
            Me.API.WriteLog($"{message}");
        }

        /// <summary>
        /// Write separator to log file.
        /// </summary>
        public void WriteSeparator()
        {
            Me.API.WriteLog($"{new string('=', 79)}");
        }

        
    }
}
