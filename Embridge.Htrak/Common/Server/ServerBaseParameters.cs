﻿using Agresso.ServerExtension;

namespace Embridge.Htrak.Common.Server
{
    internal class ServerBaseParameters : ParametersBase
    {

        #region Special parameter names
        /// <summary>
        /// Thought it might be useful to put the assembly version in to a report parameter so we can display it on reports.
        /// </summary>
        public const string AssemblyVersionCol = "_version";
        #endregion

        #region Constuctors
        public ServerBaseParameters(IReport Me) : base(Me) { }
        #endregion
    }
}
