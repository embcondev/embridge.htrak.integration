﻿using Embridge.Htrak.Common.SOAPWebservice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Embridge.Htrak.PO.SOAP
{
    public class ImportWS : SOAPBase
    {
        public string ImportWSProcessId { get; private set; }
        public string ImportWSMenuId { get; private set; }
        public int ImportWSVariantNo { get; private set; }

        public ImportWS(string username, string password, string client) : base(username, password, client)
        {
        }

        public void SetProcessDetails(string processId, string menuId, int variantNo)
        {
            ImportWSProcessId = processId;
            ImportWSMenuId = menuId;
            ImportWSVariantNo = variantNo;
        }

        public bool CallAPI(string url, string action, string cdata)
        {
            CreateWebRequest(url, action);

            MakeSOAPCall(CreateImportWSXML(cdata, ImportWSProcessId, ImportWSMenuId, ImportWSVariantNo));

            if (SOAPCallSuccess)
                GetOrderNumberFromResponse();

            return SOAPCallSuccess;
        }
    }
}
