﻿using Agresso.ServerExtension;
using Embridge.Htrak.Common.Server;
using Embridge.Htrak.PO.API;
using System;
using System.Threading;
using System.Net;
using System.Xml;
using System.IO;
using System.Xml.Schema;
using System.Text.RegularExpressions;
using Embridge.Htrak.Common.Schema;
using Embridge.Htrak.PO.SOAP;
using Embridge.Htrak.Common.SOAPWebservice;

namespace Embridge.Htrak.PO.Server
{
    using static HTPOBase;

    public class HTPO_ProcessExtender : ServerBase
    {
        internal Parameters MyParameters { get; private set; }
        private bool ProcessRunOkay = true;
        private int LG04OrderNo = 0;

        #region IServerProgram implementation
        public void Initialize(IReport report) { Me = report; }

        public void End() { }

        public void Run()
        {
            try
            {
                // Write Assembly information.
                WriteAssemblyInformation();
                // Load parameters
                MyParameters = new Parameters(Me);

                if (MyParameters.Debug)
                {
                    Me.API.WriteLog("******DEBUG - SLEEP******");
                    Thread.Sleep(20000);
                }

                if (!MyParameters.ValidateParameters())
                    ProcessRunOkay = false;

                if (ProcessRunOkay)
                    ProcessRunOkay = RunProcess();

                if (ProcessRunOkay)
                {
                    Me.SetMessage("Process completed successfully");
                }
                else
                {
                    Me.SetMessage($"An error has occurred.  See log file for details.");
                }

            }
            catch (Exception e)
            {
                Me.API.WriteLog($"{Environment.NewLine}{e.Message}{Environment.NewLine}{e.Source}{Environment.NewLine}{e.StackTrace}");

                Me.SetMessage($"An error has occurred.  See log file for details.");
            }
        }
        #endregion

        #region Private functions
        private bool RunProcess()
        {
            if (!CheckForAndCreateHTrakTable())
                return false;

            if (Guid.TryParse(MyParameters.TransactionID, out Guid transId))
                WriteMessage($"Transaction ID: {transId}, retrieving EXISTING transaction.");

            var HtrakAPI = new HTrakPO(transId);

            if (HtrakAPI.TransactionID != transId)
                WriteMessage($"Generated Transaction ID: {HtrakAPI.TransactionID}, retrieving NEW transaction.");

            var ApiCallURI = transId == Guid.Empty ? MyParameters.HTApiUrl : $"{MyParameters.HTApiUrl}{HtrakAPI.TransactionID}";

            WriteMessage($"Calling Htrack API at {ApiCallURI}");

            HtrakAPI.CallAPI(ApiCallURI, transId == Guid.Empty ? false : true);

            WriteMessage($"Response Code: {HtrakAPI.APIResponseCode}");

            if (!HtrakAPI.APICallSuccess)
            {
                WriteErrorMessage($"Response Message: {HtrakAPI.APIResponse}");
                return false;
            }

            if (MyParameters.Debug)
                WriteMessage(HtrakAPI.APIResponse);

            if (HtrakAPI.APIResponseCode == "204")
            {
                WriteMessage("No new PO information found.");
                return true;
            }

            if (!CallImportWebservice(HtrakAPI))
                return false;

            return true;
        }

        private bool CheckForAndCreateHTrakTable()
        {
            if (Me.DbAPI.IsTable("uhtrackorigporequest"))
                return true;

            var iSQL = CreateIStatement();
            iSQL.Assign("CREATE TABLE uhtrackorigporequest");
            iSQL.Append(" AS SELECT * FROM algbatchinput WHERE 1 = 2");

            try
            {
                var rowsCreated = 0;
                Me.DbAPI.ExecSql(iSQL.GetSqlString(), "Create table 'uhtrackorigporequest'", ref rowsCreated);
                return true;
            }
            catch (Exception e)
            {
                WriteErrorMessage($"CheckForAndCreateHTrakTable - Creating table 'uhtrackorigporequest': {e.Message}");
                return false;
            }
        }

        private bool CallImportWebservice(HTrakPO HtrakAPI)
        {
            WriteMessage($"Calling Import Webservice at {MyParameters.BwApiUrl}");
            WriteMessage($"Webservice UserID: {MyParameters.WSUsername}");
            WriteMessage($"Webservice Client: {MyParameters.Client}");

            if (!string.IsNullOrWhiteSpace(MyParameters.WSPassword))
                WriteMessage("Webservice Password: **********");
            else
                WriteMessage("Webservice Password: [BLANK]");
            
            var importWebService = new ImportWS(MyParameters.WSUsername, MyParameters.WSPassword, MyParameters.Client);

            if(importWebService == null)
            {
                WriteErrorMessage("Couldn't create Import Webservice connector");
                return false;
            }

            WriteMessage("Created Import Webservice connector");

            WriteMessage("Setting process details.");
            WriteMessage($"Import Process Id: {MyParameters.ImportProcessId}");
            WriteMessage($"Import Menu Id: {MyParameters.ImportMenuId}");
            WriteMessage($"Import Variant: {MyParameters.ProcessVariantId}");

            importWebService.SetProcessDetails(MyParameters.ImportProcessId, MyParameters.ImportMenuId, MyParameters.ProcessVariantId);

            if (!importWebService.ValidateImportXML(HtrakAPI.APIResponse, XMLNameSpaces.ABWOrderNameSpace, XMLSchemas.ABWOrderSchema, out string validationMessage, MyParameters.Debug))
            {
                foreach (var validationError in importWebService.SchemaValidationErrors)
                {
                    WriteMessage(validationError);
                }

                WriteErrorMessage(validationMessage);

                WriteMessage($"*********Temporarily Ignoring Validation Message***************");
                //return false;
            }
            else
            {
                WriteMessage(validationMessage);
            }



            importWebService.CallAPI(MyParameters.BwApiUrl, SOAPHelper.ImportWSAction, HtrakAPI.APIResponse);

            if (MyParameters.Debug)
            {
                WriteMessage($"{"*"}".PadRight(60, '*'));
                WriteMessage("U4ERP Import Webservice Info:");
                WriteMessage(importWebService.SOAPResponse.Replace("\n", "").Replace("\r", ""));
            }

            if(!importWebService.SOAPCallSuccess)
            {
                WriteErrorMessage("Calling Unit4 ERP Import Service.");
                WriteErrorMessage($"Message = {importWebService.SOAPResponse}");
            }

            LG04OrderNo = importWebService.OrderNo;

            if (LG04OrderNo == 0)
            {
                WriteErrorMessage("No Order Number generated for LG04, check for errors in log.");
                return false;
            }

            WriteMessage($"LG04 process ordered: {LG04OrderNo}");

            return true;
        }
        #endregion
    }
}
