﻿using Agresso.ServerExtension;
using Embridge.Htrak.PO.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Embridge.Htrak.PO
{
    using static HTPOBase;

    [ServerProgram(Id)]
    public class HTPO_SP : HTPO_ProcessExtender, IServerProgram
    {
        public new void Initialize(IReport report)
        {
            base.Initialize(report);
        }
    }
}
