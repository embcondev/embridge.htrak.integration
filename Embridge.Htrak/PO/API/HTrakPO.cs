﻿using Embridge.Htrak.Common;
using Embridge.Htrak.Common.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Embridge.Htrak.PO.API
{
    public class HTrakPO : APIBase
    {
        public Guid TransactionID { get; private set; }

        public HTrakPO(Guid transId = new Guid(), string username = default, string password = default) : base(username, password)
        {
            TransactionID = transId == Guid.Empty ? Guid.NewGuid() : transId;
        }

        public bool CallAPI(string url, bool existing = false)
        {
            CreateHttpClient(url);

            if(!existing)
                AddHeader("TRANSACTION_ID", TransactionID.ToString());

            return MakeAPIGetCall();
        }
    }
}
