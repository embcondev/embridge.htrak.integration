﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Embridge.Htrak.POUPD.DATA
{
    public class ProductLine
    {
        public int LineN { get; set; }
        public int LineH { get; set; }
        public string ProductCode { get; set; }
        public decimal UnitPrice { get; set; }
        public int Qty { get; set; }
    }

    public class FinalRequest
    {
        public FinalRequest()
        {
            ProductLines = new List<ProductLine>();
        }

        public List<ProductLine> ProductLines { get; set; }
    }

    public class POUpdateRoot
    {
        public POUpdateRoot()
        {
            FinalRequest = new FinalRequest();
        }

        public string ExtOrdId { get; set; }
        public long UBWPOId { get; set; }
        public string UBWPOType { get; set; }
        public FinalRequest FinalRequest { get; set; }
    }
}
