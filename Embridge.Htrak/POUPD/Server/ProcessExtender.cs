﻿using Agresso.Interface.CommonExtension;
using Agresso.ServerExtension;
using Embridge.Htrak.Common.Server;
using Embridge.Htrak.POUPD.API;
using Embridge.Htrak.POUPD.DATA;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Embridge.Htrak.POUPD.Server
{
    using static HTPOUBase;

    public class ProcessExtender : ServerBase
    {
        internal Parameters MyParameters { get; private set; }
        private bool ProcessRunOkay = true;

        public void RunProcess()
        {
            try
            {
                // Write Assembly information.
                WriteAssemblyInformation();
                // Load parameters
                MyParameters = new Parameters(Me);

                if (MyParameters.Debug)
                {
                    Me.API.WriteLog("******DEBUG - SLEEP******");
                    Thread.Sleep(20000);
                }

                if (!MyParameters.ValidateParameters())
                    ProcessRunOkay = false;

                if (ProcessRunOkay)
                    ProcessRunOkay = ProcessPOs();

                //if (ProcessRunOkay)
                //{
                //    Me.SetMessage("Process completed successfully");
                //}
                //else
                //{
                //    Me.SetMessage($"An error has occurred.  See log file for details.");
                //}

            }
            catch (Exception e)
            {
                Me.API.WriteLog($"{Environment.NewLine}{e.Message}{Environment.NewLine}{e.Source}{Environment.NewLine}{e.StackTrace}");

                //Me.SetMessage($"An error has occurred.  See log file for details.");
            }
        }

        private bool ProcessPOs()
        {
            if (!CreatePOsToProcess(out string hTrakPOsToProcessTableName, out int hTrakPOsToProcess))
                return false;

            if(hTrakPOsToProcess == 0)
            {
                WriteMessage("No H-Trak Purchase Orders found to process");
                return true;
            }

            var rowsToProcess = 0;

            if (!CreateBWPOData(out string helpTableName, hTrakPOsToProcessTableName, ref rowsToProcess))
                return false;

            if (!InsertDeletedPORows(helpTableName, hTrakPOsToProcessTableName, ref rowsToProcess))
                return false;

            if (rowsToProcess == 0)
            {
                WriteMessage($"No rows found to process for Order No: {MyParameters.PurchaseOrderFrom} to {MyParameters.PurchaseOrderTo}");
                return true;
            }

            if (!UpdateUnchangedRows(helpTableName))
                return false;

            var changedRows = CountChangedRows(helpTableName);

            WriteMessage($"PO Rows changed from original H-Trak Orders: {changedRows}");

            if (changedRows == 0)
            {
                WriteMessage($"No rows found changed from original H-Trak Orders");
                //return true;
            }

            if (!ProcessUpdatedOrders(helpTableName))
                return false;

            return true;
        }

        private bool ProcessUpdatedOrders(string helpTableName)
        {
            var POsToUpdate = GetPOsToUpdate(helpTableName);

            if (POsToUpdate == default)
                return false;

            foreach (DataRow purchaseOrderRow in POsToUpdate.Rows)
            {
                if (!ProcessPurchaseOrder(helpTableName, purchaseOrderRow.Field<long>("order_id")))
                    return false;
            }

            return true;
        }


        private bool ProcessPurchaseOrder(string helpTableName, long orderId)
        {
            WriteMessage($"Process PO {orderId}");
            var foundPODetails = GetPurchaseOrderDetails(helpTableName, orderId);

            if (foundPODetails.Rows.Count == 0)
            {
                WriteMessage($"No PO Details found in {helpTableName} for order id {orderId}");
                return true;
            }

            var poUpdate = GetPOUpdate(foundPODetails);

            if (poUpdate == default)
                return true;

            var poUpdateJson = JsonConvert.SerializeObject(poUpdate);

            if (MyParameters.Debug)
            {
                var jsonCharsSize = poUpdateJson.Length.ToString();
                var jsonMbSize = ((decimal)Encoding.Unicode.GetByteCount(poUpdateJson) / 1048576).ToString();

                WriteMessage($"Current size of the Json string is {jsonCharsSize} chars and {jsonMbSize} Mbs.");

                WriteMessage($"{"*"}".PadRight(60, '*'));
                WriteMessage($"** Below the JSON Payload for PO Order {orderId} **");
                WriteMessage(poUpdateJson);
                WriteMessage("** End of the JSON **");
                WriteMessage($"{"*"}".PadRight(60, '*'));
            }

            var HtrakAPI = new HTrakPOU();

            WriteMessage($"Calling Htrack API at {MyParameters.HTApiUrl}");

            HtrakAPI.CallAPI(MyParameters.HTApiUrl, JsonConvert.SerializeObject(poUpdate));

            WriteMessage($"Response Code: {HtrakAPI.APIResponseCode}");

            if (!HtrakAPI.APICallSuccess)
            {
                WriteErrorMessage($"Response Message: {HtrakAPI.APIResponse}");
                return false;
            }

            if (MyParameters.Debug)
                WriteMessage(HtrakAPI.APIResponse);

            return true;
        }

        private POUpdateRoot GetPOUpdate(DataTable poDetails)
        {
            WriteMessage($"Create JSON for H-Trak");

            if (poDetails.Rows.Count == 0)
            {
                WriteMessage("No PO Details found.");
                return default;
            }

            //if (!long.TryParse(poDetails.Rows[0].Field<string>("ExtOrdId"), out long foundExtOrdId))
            //{
            //    WriteErrorMessage($"ExtOrdId is '{poDetails.Rows[0].Field<string>("ExtOrdId")}' which is not a vlaid number.");
            //}

            var poUpdateRoot = new POUpdateRoot()
            {
                ExtOrdId = poDetails.Rows[0].Field<string>("ExtOrdId"),
                UBWPOId = poDetails.Rows[0].Field<long>("UBWPOId"),
                UBWPOType = poDetails.Rows[0].Field<string>("UBWPOType")
            };


            foreach (DataRow currentRow in poDetails.Rows)
            {
                var productLine = new ProductLine()
                {
                    LineN = currentRow.Field<int>("LineN"),
                    LineH = currentRow.Field<int>("LineH"),
                    ProductCode = currentRow.Field<string>("ProductCode"),
                    UnitPrice = currentRow.Field<decimal>("UnitPrice"),
                    Qty = (int)currentRow.Field<decimal>("Qty")
                };

                poUpdateRoot.FinalRequest.ProductLines.Add(productLine);
            }

            return poUpdateRoot;
        }

        private DataTable GetPurchaseOrderDetails(string helpTableName, long orderId)
        {
            var iSQL = CreateIStatement();
            iSQL.Assign("SELECT ext_order_id AS ExtOrdId");
            iSQL.Append(", order_id AS UBWPOId");
            iSQL.Append(", voucher_type AS UBWPOType");
            iSQL.Append(", line_no AS LineN");
            iSQL.Append(", ht_line_no AS LineH");
            iSQL.Append(", CASE WHEN article = '' THEN ht_article ELSE article END AS ProductCode");
            iSQL.Append(", CASE WHEN line_no = 0 THEN ht_unit_price ELSE unit_price END AS UnitPrice");
            iSQL.Append(", CASE WHEN line_no = 0 THEN ht_value_1 ELSE value_1 END AS Qty");
            iSQL.Append($" FROM {helpTableName}");
            iSQL.Append(" WHERE order_id = @orderId");
            iSQL.Append(" ORDER BY line_no");
            iSQL.SetParameter("orderId", orderId);

            var SQLString = iSQL.GetSqlString();

            var foundPODetails = new DataTable();

            try
            {
                CurrentContext.Database.Read(iSQL, foundPODetails);
            }
            catch (Exception e)
            {
                WriteErrorMessage($"GetPurchaseOrderDetails - Getting PO Details from {helpTableName}: {e.Message}");
            }

            return foundPODetails;
        }

        private bool CreatePOsToProcess(out string helpTableName, out int foundHTrakRows)
        {
            helpTableName = Me.DbAPI.GetNextTempTableName();
            foundHTrakRows = 0;

            var reportHelpTableName = string.Empty;

            if (Me.API.GetReportParameterNames().Contains("hlptab"))
                reportHelpTableName = Me.API.GetReportParameter("hlptab");

            var iSQL = CreateIStatement();
            iSQL.Assign("SELECT DISTINCT(h.order_id) AS order_id");
            iSQL.Append(" FROM apoheader h");

            //We need to catch instance where a PO01 or PO03 was ordered but that report did not process any purchase orders
            if (string.IsNullOrWhiteSpace(reportHelpTableName) && Me.ReportName != Id)
            {
                iSQL.Append(" WHERE 1=2");
            }
            else
            {
                iSQL.Append(" INNER JOIN uhtrackorigporequest u ON u.client = h.client AND u.order_id = h.order_id");

                if (!string.IsNullOrWhiteSpace(reportHelpTableName))
                    iSQL.Append($" INNER JOIN {reportHelpTableName} hlp ON h.client = hlp.client AND h.order_id = hlp.order_id");

                iSQL.Append(" WHERE h.client = @client");
                iSQL.Append(" AND h.order_id BETWEEN @orderNoFrom AND @orderNoTo");
                iSQL.SetParameter("orderNoFrom", MyParameters.PurchaseOrderFrom);
                iSQL.SetParameter("orderNoTo", MyParameters.PurchaseOrderTo);
                iSQL.SetParameter("client", MyParameters.Client);
            }

            try
            {
                Me.DbAPI.CreateTable(helpTableName, iSQL.GetSqlString(), $"Create Help Table: {helpTableName}", ref foundHTrakRows);
                return true;
            }
            catch (Exception e)
            {
                WriteErrorMessage($"CreatePOsToProcess - Creating Help Table {helpTableName}: {e.Message}");
                return false;
            }
        }

        private bool CreateBWPOData(out string helpTableName, string hTrakPOsToProcessTableName, ref int rowsToProcess)
        {
            helpTableName = Me.DbAPI.GetNextTempTableName();

            var iSQL = CreateIStatement();
            iSQL.Assign("SELECT h.voucher_type, h.order_id, h.ext_order_id, d.line_no, d.article, d.rev_price AS unit_price, d.rev_val AS value_1, IFNULL(u.line_no, -1) AS ht_line_no, IFNULL(u.article, '') AS ht_article, IFNULL(u.unit_price, 0) AS ht_unit_price, IFNULL(u.value_1, 0) AS ht_value_1, 1 as diff");
            iSQL.Append(" FROM apodetail d");
            iSQL.Append($" INNER JOIN {hTrakPOsToProcessTableName} ht ON d.order_id = ht.order_id");
            iSQL.Append(" INNER JOIN apoheader h ON h.order_id = d.order_id AND h.client = d.client");
            iSQL.Append(" LEFT JOIN uhtrackorigporequest u ON u.client = d.client AND u.order_id = d.order_id AND u.line_no = d.line_no");
            iSQL.Append(" WHERE d.client = @client");
            iSQL.Append(" AND d.status != 'T'");
            iSQL.SetParameter("client", MyParameters.Client);

            try
            {
                int rowsFound = 0;
                Me.DbAPI.CreateTable(helpTableName, iSQL.GetSqlString(), $"Create Help Table: {helpTableName}", ref rowsFound);
                rowsToProcess += rowsFound;
                return true;
            }
            catch (Exception e)
            {
                WriteErrorMessage($"CreateBWPOData - Creating Help Table {helpTableName}: {e.Message}");
                return false;
            }
        }

        private bool InsertDeletedPORows(string helpTableName, string hTrakPOsToProcessTableName, ref int rowsToProcess)
        {
            var iSQL = CreateIStatement();
            iSQL.Assign($"INSERT INTO {helpTableName} (voucher_type, order_id, ext_order_id, line_no, article, unit_price, value_1, ht_line_no, ht_article, ht_unit_price, ht_value_1, diff)");
            iSQL.Append(" SELECT h.voucher_type");
            iSQL.Append(", u.order_id");
            iSQL.Append(", u.ext_order_id");
            iSQL.Append(", 0 AS line_no");
            iSQL.Append(", '' AS article");
            iSQL.Append(", 0 AS unit_price");
            iSQL.Append(", 0 AS value_1");
            iSQL.Append(", u.line_no AS ht_line_no");
            iSQL.Append(", u.article AS ht_article");
            iSQL.Append(", u.unit_price AS ht_unit_price");
            iSQL.Append(", u.value_1 AS ht_value_1");
            iSQL.Append(", 1 AS diff");
            iSQL.Append(" FROM uhtrackorigporequest u");
            iSQL.Append($" INNER JOIN {hTrakPOsToProcessTableName} ht ON u.order_id = ht.order_id");
            iSQL.Append(" INNER JOIN apoheader h ON h.order_id = u.order_id AND h.client = u.client");
            iSQL.Append(" WHERE u.client = @client");
            iSQL.Append(" AND u.line_no != 0");
            iSQL.Append(" AND EXISTS(SELECT d.line_no FROM apodetail d WHERE d.line_no = u.line_no AND d.order_id = u.order_id AND d.status = 'T')");
            iSQL.SetParameter("client", MyParameters.Client);

            try
            {
                int rowsFound = 0;
                Me.DbAPI.ExecSql(iSQL.GetSqlString(), $"Insert deleted PO rows into {helpTableName}", ref rowsFound);
                rowsToProcess += rowsFound;
                return true;
            }
            catch (Exception e)
            {
                WriteErrorMessage($"InsertDeletedPORows - Inserting deleted PO rows into {helpTableName}: {e.Message}");
                return false;
            }
        }

        private bool UpdateUnchangedRows(string helpTableName)
        {
            var setString = "SET diff = 0";
            var whereString = "WHERE diff = 1 AND line_no = ht_line_no AND article = ht_article AND unit_price = ht_unit_price AND value_1 = ht_value_1";

            try
            {
                Me.DbAPI.UpdateTable(helpTableName, string.Empty, setString, whereString, string.Empty, $"Update unchanged rows on {helpTableName}");
                return true;
            }
            catch (Exception e)
            {
                WriteErrorMessage($"UpdateUnchangedRows - Updating unchanged rows on {helpTableName}: {e.Message}");
                return false;
            }
        }

        private int CountChangedRows(string helpTableName)
        {
            var iSQL = CreateIStatement();

            iSQL.Assign($"SELECT COUNT(*) AS changecount FROM {helpTableName} WHERE diff = 1");

            try
            {
                var changedRows = 0;
                if (CurrentContext.Database.ReadValue(iSQL, ref changedRows))
                    return changedRows;
            }
            catch (Exception e)
            {
                WriteErrorMessage($"CountChangedRows - Counting changed rows on {helpTableName}: {e.Message}");
            }

            return 0;
        }

        private DataTable GetPOsToUpdate(string helpTableName)
        {
            var foundPOsToUpdate = new DataTable();

            var iSQL = CreateIStatement();
            iSQL.Assign("SELECT DISTINCT order_id");
            iSQL.Append($" FROM {helpTableName}");
            //iSQL.Append(" WHERE diff = 1");

            try
            {
                CurrentContext.Database.Read(iSQL, foundPOsToUpdate);
                return foundPOsToUpdate;
            }
            catch (Exception e)
            {
                WriteErrorMessage($"GetPOsToUpdate - Getting POs to update from {helpTableName}: {e.Message}");
                return default;
            }
        }
    }
}
