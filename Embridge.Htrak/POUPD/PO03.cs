﻿using Agresso.ServerExtension;
using Embridge.Htrak.POUPD.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Embridge.Htrak.POUPD
{
    [Report("PO03", "*", "H-Trak PO Update", -1)]
    public class PO03 : ProcessExtender, IProjectServer
    {
        public void Initialize(IReport report)
        {
            Me = report;

            if (Me != default)
            {
                report.OnStop += OnStop;
            }
        }

        private void OnStop(object sender, ReportEventArgs e)
        {
            RunProcess();
        }
    }
}
