﻿using Agresso.ServerExtension;
using Embridge.Htrak.POUPD.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Embridge.Htrak.POUPD
{
    using static HTPOUBase;

    [ServerProgram(Id)]
    public class HTPRD_SP : ProcessExtender, IServerProgram
    {
        public void Initialize(IReport report)
        {
            Me = report;
        }

        public void Run()
        {
            RunProcess();
        }

        public void End() { }
    }
}
