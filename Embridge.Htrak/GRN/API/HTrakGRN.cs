﻿using Embridge.Htrak.Common.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Embridge.Htrak.GRN.API
{
    public class HTrakGRN : APIBase
    {
        public HTrakGRN(string username = default, string password = default) : base(username, password)
        {

        }

        public bool CallAPI(string url)
        {
            CreateHttpClient(url);

            return MakeAPIGetCall();
        }
    }
}
