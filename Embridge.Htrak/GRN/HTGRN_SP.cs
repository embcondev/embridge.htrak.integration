﻿using Agresso.ServerExtension;
using Embridge.Htrak.GRN.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Embridge.Htrak.GRN
{
    using static HTGRNBase;

    [ServerProgram(Id)]
    public class HTGRN_SP : HTGRN_ProcessExtender, IServerProgram
    {
        public new void Initialize(IReport report)
        {
            base.Initialize(report);
        }
    }
}
