﻿using Agresso.ServerExtension;
using Embridge.Htrak.Common;
using Embridge.Htrak.Common.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Embridge.Htrak.GRN.Server
{
    public class HTGRNBase
    {
        public const string Description = "GRN Import";
        public const string Id = "HTGRN";

        internal class Parameters : ParametersBase
        {
            #region Parameter names
            public const string ImportProcessIdParam = "imp_proc_id";
            public const string ImportMenuIdParam = "imp_menu_id";
            public const string ProcessVariantIdParam = "imp_var_id";
            public const string DebugParam = "debug";
            #endregion

            #region Parameter Values
            public string HTApiUrl { get => GetHTAPIAddress(); }
            public string BwApiUrl { get => GetU4WSAddress(); }
            public string ImportProcessId { get => GetString(ImportProcessIdParam); }
            public string ImportMenuId { get => GetString(ImportMenuIdParam); }
            public int ProcessVariantId { get => GetInt(ProcessVariantIdParam); }
            public string WSUsername { get => GetU4WSUsername(); }
            public string WSPassword { get => GetU4WSPassword(); }
            public bool Debug { get => GetBool(DebugParam, false); }
            #endregion

            #region Other Values
            public SysSetupValueList HTApiSysSetup { get; private set; }
            public SysSetupValueList U4WSSysSetup { get; private set; }
            #endregion

            #region Constructors
            public Parameters(IReport Me) : base(Me)
            {
                HTApiSysSetup = new SysSetupValueList();
                U4WSSysSetup = new SysSetupValueList();
            }
            #endregion

            public bool ValidateParameters()
            {
                bool validParameters = true;

                HTApiSysSetup = GetSysSetupValues(SystemSetupValues.EK_HTRAK_API, Me.SysSetupCode);
                U4WSSysSetup = GetSysSetupValues(SystemSetupValues.EK_HTRAK_U4WS, Me.SysSetupCode);

                if (string.IsNullOrWhiteSpace(HTApiUrl))
                    validParameters = false;

                if (string.IsNullOrWhiteSpace(BwApiUrl))
                    validParameters = false;

                if (HTApiSysSetup.SysSetupValues.Count < 1)
                {
                    Me.API.WriteLog($"ERROR: No {SystemSetupValues.EK_HTRAK_API} System Setup Values configured");
                    validParameters = false;
                }

                if (U4WSSysSetup.SysSetupValues.Count < 1)
                {
                    Me.API.WriteLog($"ERROR: No {SystemSetupValues.EK_HTRAK_U4WS} System Setup Values configured");
                    validParameters = false;
                }

                return validParameters;
            }

            private string GetHTAPIAddress()
            {
                var sysSetupValue = HTApiSysSetup.SysSetupValues.FirstOrDefault(s => s.Text1 == Id);

                if (sysSetupValue != default)
                    return sysSetupValue.Description;

                return string.Empty;
            }

            private string GetU4WSAddress()
            {
                var sysSetupValue = U4WSSysSetup.SysSetupValues.FirstOrDefault(s => s.Text1 == "IMPORT");

                if (sysSetupValue != default)
                    return sysSetupValue.Description;

                return string.Empty;
            }

            private string GetU4WSUsername()
            {
                var sysSetupValue = U4WSSysSetup.SysSetupValues.FirstOrDefault(s => s.Text1 == "IMPORT");

                if (sysSetupValue != default)
                    return sysSetupValue.Text2;

                return string.Empty;
            }

            private string GetU4WSPassword()
            {
                var sysSetupValue = U4WSSysSetup.SysSetupValues.FirstOrDefault(s => s.Text1 == "IMPORT");

                if (sysSetupValue != default)
                    return sysSetupValue.Text3;

                return string.Empty;
            }
        }
    }
}
