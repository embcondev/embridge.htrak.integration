﻿using Agresso.ServerExtension;
using Embridge.Htrak.Common.Schema;
using Embridge.Htrak.Common.Server;
using Embridge.Htrak.Common.SOAPWebservice;
using Embridge.Htrak.GRN.API;
using Embridge.Htrak.GRN.SOAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Embridge.Htrak.GRN.Server
{
    using static HTGRNBase;

    public class HTGRN_ProcessExtender : ServerBase
    {
        internal Parameters MyParameters { get; private set; }
        private bool ProcessRunOkay = true;
        private int LG04OrderNo = 0;

        #region IServerProgram implementation
        public void Initialize(IReport report) { Me = report; }

        public void End() { }

        public void Run()
        {
            try
            {
                // Write Assembly information...
                WriteAssemblyInformation();
                // Load parameters...
                MyParameters = new Parameters(Me);

                if (MyParameters.Debug)
                {
                    Me.API.WriteLog("******DEBUG - SLEEP******");
                    Thread.Sleep(20000);
                }

                if (!MyParameters.ValidateParameters())
                    ProcessRunOkay = false;

                if (ProcessRunOkay)
                    ProcessRunOkay = RunProcess();

                if (ProcessRunOkay)
                {
                    Me.SetMessage("Process completed successfully");
                }
                else
                {
                    Me.SetMessage($"An error has occurred.  See log file for details.");
                }

            }
            catch (Exception e)
            {
                Me.API.WriteLog($"{Environment.NewLine}{e.Message}{Environment.NewLine}{e.Source}{Environment.NewLine}{e.StackTrace}");

                Me.SetMessage($"An error has occurred.  See log file for details.");
            }
        }
        #endregion

        #region Private functions
        private bool RunProcess()
        {
            var HtrakAPI = new HTrakGRN();

            WriteMessage($"Calling Htrack API at {MyParameters.HTApiUrl}");

            HtrakAPI.CallAPI(MyParameters.HTApiUrl);

            WriteMessage($"Response Code: {HtrakAPI.APIResponseCode}");

            if (!HtrakAPI.APICallSuccess)
            {
                WriteErrorMessage($"Response Message: {HtrakAPI.APIResponse}");
                return false;
            }

            if (MyParameters.Debug)
                WriteMessage(HtrakAPI.APIResponse);

            if (HtrakAPI.APIResponseCode == "204")
            {
                WriteMessage("No new GRN information found.");
                return true;
            }

            if (!CallImportWebservice(HtrakAPI))
                return false;

            return true;
        }

        private bool CallImportWebservice(HTrakGRN HtrakAPI)
        {
            WriteMessage($"Calling Import Webservice at {MyParameters.BwApiUrl}");

            var importWebService = new ImportWS(MyParameters.WSUsername, MyParameters.WSPassword, MyParameters.Client);

            importWebService.SetProcessDetails(MyParameters.ImportProcessId, MyParameters.ImportMenuId, MyParameters.ProcessVariantId);

            if (!importWebService.ValidateImportXML(HtrakAPI.APIResponse, XMLNameSpaces.ABWGRNNameSpace, XMLSchemas.ABWGRNSchema, out string validationMessage, MyParameters.Debug))
            {
                foreach (var validationError in importWebService.SchemaValidationErrors)
                {
                    WriteMessage(validationError);
                }

                WriteErrorMessage(validationMessage);

                WriteMessage($"*********Temporarily Ignoring Validation Message***************");
                //return false;
            }
            else
            {
                WriteMessage(validationMessage);
            }

            importWebService.CallAPI(MyParameters.BwApiUrl, SOAPHelper.ImportWSAction, HtrakAPI.APIResponse);

            if (MyParameters.Debug)
            {
                WriteMessage($"{"*"}".PadRight(60, '*'));
                WriteMessage("U4ERP Import Webservice Info:");
                WriteMessage(importWebService.SOAPResponse.Replace("\n", "").Replace("\r", ""));
            }

            if (!importWebService.SOAPCallSuccess)
            {
                WriteErrorMessage("Calling Unit4 ERP Import Service.");
                WriteErrorMessage($"Message = {importWebService.SOAPResponse}");
            }

            LG04OrderNo = importWebService.OrderNo;

            if (LG04OrderNo == 0)
            {
                WriteErrorMessage("No Order Number generated for LG04, check for errors in log.");
                return false;
            }

            WriteMessage($"LG04 process ordered: {LG04OrderNo}");

            return true;
        }

        #endregion
    }
}
