- asql.lst file should list the complete filenames of the scripts you want to run.
- edit the contents of 'update_version_information.asq' file with your Customer code, Customization description and version number.
- make sure you put the file 'update_version_information.asq' in last.


Example of asql.lst contents:

CREATE Table MyTable1.asq
UPDATES SystemParameters 2018.asq
update_version_information.asq