﻿SELECT DISTINCT 
	a.article as u4_nsv_code,
	a.art_descr as u4_nsv_descr, 
	CASE WHEN r.rel_value IS NULL THEN 'N/A' ELSE r.rel_value END as supp_prod_code,
	a.apar_id as supplier_id, 
	CASE WHEN s.apar_name IS NULL THEN 'N/A' ELSE s.apar_name END as supplier_descr,
	LEFT(a.article,3) as e_class_code,
	'' as  e_class_descr,
	a.unit_code as unit_issue, 
	CASE WHEN um.description IS NULL THEN 'N/A' ELSE um.description END as unit_measure,
	'' as stock_type, 
	CASE WHEN p.unit_price IS NULL THEN 0.00 ELSE p.unit_price END as purchase_each_price,
	2779 as req_point, 
	a.art_gr_id as acc_finance_code,
	a.client

FROM algarticle a
	LEFT OUTER JOIN asuheader s ON (s.apar_id = a.apar_id)
	LEFT OUTER JOIN algunit um ON (um.client = a.client AND um.article_id = a.article_id)
	LEFT OUTER JOIN apoprice p ON (p.client = a.client AND p.apar_id = a.apar_id AND p.article_id = a.article_id)
	LEFT OUTER JOIN algrelvalue r ON (r.client = a.client AND r.article_id = a.article_id AND r.rel_attr_id = 'V030')
	INNER JOIN algrelvalue r2 ON (r2.client = a.client AND r2.article_id = a.article_id AND r2.rel_attr_id = 'V120')

WHERE  
a.client IN ('2S')