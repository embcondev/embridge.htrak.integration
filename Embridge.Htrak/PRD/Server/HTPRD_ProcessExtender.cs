﻿using Agresso.Interface.CommonExtension;
using Agresso.ServerExtension;
using Embridge.Htrak.Common.Server;
using Embridge.Htrak.PRD.API;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Embridge.Htrak.PRD.Server
{
    using static HTPRDBase;

    public class HTPRD_ProcessExtender : ServerBase
    {
        internal Parameters MyParameters { get; private set; }
        private bool ProcessRunOkay = true;
        private string ProductJsonName = "ABWProducts";

        #region IServerProgram implementation
        public void Initialize(IReport report) { Me = report; }

        public void End() { }

        public void Run()
        {
            try
            {
                // Write Assembly information.
                WriteAssemblyInformation();
                // Load parameters
                MyParameters = new Parameters(Me);

                if (MyParameters.Debug)
                {
                    Me.API.WriteLog("******DEBUG - SLEEP******");
                    Thread.Sleep(20000);
                }

                if (!MyParameters.ValidateParameters())
                    ProcessRunOkay = false;

                if (ProcessRunOkay)
                    ProcessRunOkay = RunProcess();

                if (ProcessRunOkay)
                {
                    Me.SetMessage("Process completed successfully");
                }
                else
                {
                    Me.SetMessage($"An error has occurred.  See log file for details.");
                }

            }
            catch (Exception e)
            {
                Me.API.WriteLog($"{Environment.NewLine}{e.Message}{Environment.NewLine}{e.Source}{Environment.NewLine}{e.StackTrace}");

                Me.SetMessage($"An error has occurred.  See log file for details.");
            }
        }
        #endregion

        #region Private functions
        private bool RunProcess()
        {
            if (!ViewExists())
                return false;

            var ProductDataTable = GetProductData();

            if(ProductDataTable.Rows.Count == 0)
            {
                WriteMessage("No Product rows found to export");
                return true;
            }

            var productJson = GetJsonFromDataTable(ProductDataTable);

            if (string.IsNullOrWhiteSpace(productJson))
            {
                WriteErrorMessage("Created JSON is empty");
                return false;
            }

            var HtrakAPI = new HTrakPRD();

            WriteMessage($"Calling Htrack API at {MyParameters.HTApiUrl}");

            HtrakAPI.CallAPI(MyParameters.HTApiUrl, productJson);

            WriteMessage($"Response Code: {HtrakAPI.APIResponseCode}");

            if (!HtrakAPI.APICallSuccess)
            {
                WriteErrorMessage($"Response Message: {HtrakAPI.APIResponse}");
                return false;
            }

            if (MyParameters.Debug)
                WriteMessage(HtrakAPI.APIResponse);

            return true;
        }

        private string GetJsonFromDataTable(DataTable dataTable)
        {
            WriteMessage($"Creating {ProductJsonName} JSON");
            var dataSet = new DataSet();
            dataSet.Tables.Add(dataTable);

            var productJson = JsonConvert.SerializeObject(dataSet, Formatting.None);

            if(MyParameters.Debug)
            {
                var jsonCharsSize = productJson.Length.ToString();
                var jsonMbSize = ((decimal)Encoding.Unicode.GetByteCount(productJson) / 1048576).ToString();

                WriteMessage($"Current size of the Json string is {jsonCharsSize} chars and {jsonMbSize} Mbs.");

                WriteMessage($"{"*"}".PadRight(60, '*'));
                WriteMessage("** Below the JSON Payload **");
                WriteMessage(productJson);
                WriteMessage("** End of the JSON **");
                WriteMessage($"{"*"}".PadRight(60, '*'));
            }

            return productJson;
        }

        private DataTable GetProductData()
        {
            var abwProductsTable = new DataTable(ProductJsonName);
            var iSQL = CreateIStatement();

            iSQL.Assign("SELECT *");
            iSQL.Append($" FROM {MyParameters.HTProdView}");
            iSQL.Append(" WHERE client = @client");
            iSQL.SetParameter("client", MyParameters.Client);

            try
            {
                CurrentContext.Database.Read(iSQL, abwProductsTable);

                if(abwProductsTable.Columns.Contains("client"))
                {
                    abwProductsTable.Columns.Remove("client");
                }
            }
            catch (Exception e)
            {
                WriteErrorMessage($"GetProductData - Getting product data from '{MyParameters.HTProdView}': {e.Message}");
            }

            return abwProductsTable;
        }

        private bool ViewExists()
        {
            var iSQL = CreateIStatement();
            iSQL.Assign($"SELECT client FROM {MyParameters.HTProdView} WHERE 1=2");


            try
            {
                var rowsFound = 0;
                Me.DbAPI.ExecSql(iSQL.GetSqlString(), $"Checking view '{MyParameters.HTProdView}' exists", ref rowsFound);
                return true;
            }
            catch (Exception e)
            {
                WriteErrorMessage($"ViewExists - Exception: {e.Message}");
            }

            WriteErrorMessage($"View '{MyParameters.HTProdView}' does not exist");
            return false;
        }

        #endregion
    }
}
