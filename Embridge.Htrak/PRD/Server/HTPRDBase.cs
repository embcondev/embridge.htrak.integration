﻿using Agresso.ServerExtension;
using Embridge.Htrak.Common;
using Embridge.Htrak.Common.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Embridge.Htrak.PRD.Server
{
    public class HTPRDBase
    {
        public const string Description = "Product Export";
        public const string Id = "HTPRD";

        internal class Parameters : ParametersBase
        {
            #region Parameter names
            public const string HTProdViewParam = "ht_prod_view";
            public const string DebugParam = "debug";
            #endregion

            #region Parameter Values
            public string HTApiUrl { get => GetHTAPIAddress(); }
            public string HTProdView { get => GetString(HTProdViewParam); }
            public bool Debug { get => GetBool(DebugParam, false); }
            #endregion

            #region Other Values
            public SysSetupValueList HTApiSysSetup { get; private set; }
            #endregion

            #region Constructors
            public Parameters(IReport Me) : base(Me)
            {
                HTApiSysSetup = new SysSetupValueList();
            }
            #endregion

            public bool ValidateParameters()
            {
                bool validParameters = true;

                HTApiSysSetup = GetSysSetupValues(SystemSetupValues.EK_HTRAK_API, Me.SysSetupCode);

                if (string.IsNullOrWhiteSpace(HTApiUrl))
                {
                    Me.API.WriteLog($"ERROR: No {SystemSetupValues.EK_HTRAK_API} System Setup Value configured for {Id}");
                    validParameters = false;
                }

                if (HTApiSysSetup.SysSetupValues.Count < 1)
                {
                    Me.API.WriteLog($"ERROR: No {SystemSetupValues.EK_HTRAK_API} System Setup Values configured");
                    validParameters = false;
                }

                return validParameters;
            }

            private string GetHTAPIAddress()
            {
                var sysSetupValue = HTApiSysSetup.SysSetupValues.FirstOrDefault(s => s.Text1 == Id);

                if (sysSetupValue != default)
                    return sysSetupValue.Description;

                return string.Empty;
            }
        }
    }
}
