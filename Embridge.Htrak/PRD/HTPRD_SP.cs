﻿using Agresso.ServerExtension;
using Embridge.Htrak.PRD.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Embridge.Htrak.PRD
{
    using static HTPRDBase;

    [ServerProgram(Id)]
    public class HTPRD_SP : HTPRD_ProcessExtender, IServerProgram
    {
        public new void Initialize(IReport report)
        {
            base.Initialize(report);
        }
    }
}
