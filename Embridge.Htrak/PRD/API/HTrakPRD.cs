﻿using Embridge.Htrak.Common.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Embridge.Htrak.PRD.API
{
    public class HTrakPRD : APIBase
    {
        public HTrakPRD(string username = default, string password = default) : base(username, password)
        {

        }

        public bool CallAPI(string url, string jsonContent)
        {
            CreateHttpClient(url);

            return MakeAPIPOSTCall(jsonContent);
        }
    }
}
